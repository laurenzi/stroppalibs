// Delay Patcher Creater
// Written by Arshia Cont, Carlo Laurenzi
// August 2015
// See https://forge.ircam.fr/p/StroppaLibs/doc/ for more details

// Inlets and outlets
inlets = 1;
outlets = 0;

// global variables
var numberDelays = 8 ;
var theNameSpace = 'del'; 

// parent elements
var theMainInlet;
var theMainOutletL;
var theMainOutletR;
var DelPatchers = new Array(64); 
var GlobalGainReceive;
var GlobalGainMultR;
var GlobalGainMultL;
var GlobalGainMess;
var GlobalGainLine;
var theReceiveTilde;
var theSendTildeL;
var theSendTildeR;
var theDbConvertor;
var GlobalMeterL;
var GlobalMeterR;
var GlobalScalerL;
var GlobalScalerR;


// subpatcher elements
var AudioInlet = new Array(64);
var AudioOutlets = new Array(64);
var theGainReceivers = new Array(64);
var theDbConvertors = new Array(64);


//// Instantiation arguments
// Arguments in order are:  #ofDelays #NamSpace
if (jsarguments.length>=2)
{   
	NameSpace(jsarguments[2]); // 2st argument is namespace
	Delays(jsarguments[1]); // 1st argument is numberDelays (that builds the patch)
}

function Delays(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];
		
		// safety check for number of delays
		if(a<2) a = 2; // too few Dels, set to 2		
		
		// Delete older stuff if any
		this.patcher.remove(theMainInlet);
		this.patcher.remove(theMainOutletL);
		this.patcher.remove(theMainOutletR);
		this.patcher.remove(GlobalGainMultL);
		this.patcher.remove(GlobalGainMultR);
		this.patcher.remove(GlobalGainReceive);
		this.patcher.remove(GlobalGainMess);
		this.patcher.remove(GlobalGainLine);
		this.patcher.remove(theDbConvertors);
		this.patcher.remove(theSendTildeL);
		this.patcher.remove(theSendTildeR);
		this.patcher.remove(theReceiveTilde);
		this.patcher.remove(GlobalMeterL);
		this.patcher.remove(GlobalMeterR);
		this.patcher.remove(GlobalScalerL);
		this.patcher.remove(GlobalScalerR);
					
		for (k=0;k<numDelays;k++)
		{
			this.patcher.remove(DelPatchers[k]);
		}
		
		// Create patcher
		var numDelays = numberDelays;
		numDelays = a;
		theMainInlet = this.patcher.newdefault(20,100, "inlet");
		theMainOutletL = this.patcher.newdefault(20,300, "outlet");
		theMainOutletR = this.patcher.newdefault(200,300, "outlet");
		var sendtl = theNameSpace+'_out-L';
		var sendtr = theNameSpace+'_out-R';
		theSendTildeL = this.patcher.newdefault(70, 300, "send~", sendtl);
		theSendTildeR = this.patcher.newdefault(250, 300, "send~", sendtr);
		
		var sin = theNameSpace+'_in'; // receive~ input name
		theReceiveTilde = this.patcher.newdefault(220, 90, "receive~", sin);
		var scalervalue = (1.0/numDelays);
		GlobalScalerL = this.patcher.newdefault(20, 220, "*~", scalervalue);
		GlobalScalerR = this.patcher.newdefault(200, 220, "*~", scalervalue);
		
		// Global Gain Control
		var ggainrec = theNameSpace+'_out_gain';
		GlobalGainReceive = this.patcher.newdefault(500,25, "receive", ggainrec );
		theDbConvertor = this.patcher.newdefault(500, 50, "dbtoa");
		GlobalGainMess = this.patcher.newobject("message",500, 75,90,15, "$1 30");
		GlobalGainLine = this.patcher.newdefault(500, 100, "line~");
		GlobalGainMultL = this.patcher.newdefault(20,250, "*~", 0.01);
		GlobalGainMultR = this.patcher.newdefault(200,250, "*~", 0.01);
		this.patcher.connect(GlobalGainReceive, 0, theDbConvertor, 0);
		this.patcher.connect(theDbConvertor, 0, GlobalGainMess, 0);
		this.patcher.connect(GlobalGainMess, 0, GlobalGainLine, 0);
		this.patcher.connect(GlobalGainLine, 0, GlobalGainMultL, 1);
		this.patcher.connect(GlobalGainLine, 0, GlobalGainMultR, 1);
			
		GlobalMeterL = this.patcher.newdefault(20, 270, "meter~");
		GlobalMeterR = this.patcher.newdefault(200, 270, "meter~");
			
		
		for (i=0; i<numDelays; i++)
		{
			var snum =  theNameSpace+(i+1); // patcher name
			DelPatchers[i] = this.patcher.newdefault(20+(i*100),175, "auras.delv.02~", snum);
			
			// Create connections
			this.patcher.connect(theReceiveTilde, 0, DelPatchers[i], 0);
			this.patcher.connect(theMainInlet, 0, DelPatchers[i], 0);
			this.patcher.connect(DelPatchers[i], 0, GlobalScalerL, 0);
			this.patcher.connect(DelPatchers[i], 1, GlobalScalerR, 0);
			this.patcher.connect(GlobalScalerL, 0, GlobalGainMultL, 0);
			this.patcher.connect(GlobalScalerR, 0, GlobalGainMultR, 0);
			this.patcher.connect(GlobalGainMultL, 0, theMainOutletL, 0);
			this.patcher.connect(GlobalGainMultR, 0, theMainOutletR, 0);
			this.patcher.connect(GlobalGainMultL, 0, theSendTildeL, 0);
			this.patcher.connect(GlobalGainMultR, 0, theSendTildeR, 0);
			this.patcher.connect(GlobalGainMultL, 0, GlobalMeterL, 0);
			this.patcher.connect(GlobalGainMultR, 0, GlobalMeterR, 0);
		}		
		
	}else
	{
		post("Delays message needs arguments");
		post();
	}
}



function NameSpace(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];

		theNameSpace = a;
	}else
	{
		post("SendName message needs arguments");
		post();
	}
}