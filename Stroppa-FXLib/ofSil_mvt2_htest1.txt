BPM   148.0 

VARIANCE 0.3		; was 0.5


@transpose 0

$diapason := 440.0

antescofo::analysis 8192 1024
antescofo::tune $diapason


;------------- Definition of AutoWah:
; A classic midi to hz convertor
curve wahwah-out -96, -96 60ms
wah-Q 6.0
wah-filter gainresonant
$diapason := 440
@FUN_DEF midicent2hz($X)  {
	$diapason* @exp( ( ($X/100.0)-69.0) * @log(2.0)/12)
}

@MACRO_DEF wahwah($p,$dur)
{
	curve wahwah-out 0.0 60ms

	abort autowah
	curve autowah @action := wah-params $freq $gain, @grain:=0.01s
	{
		$freq
		{
			{ (@midicent2hz($p)) }	;@type "exp_out"
			$dur/2  	{ (@midicent2hz($p+2400)) }	;@type "exp_out"
			$dur/2 	{	(@midicent2hz($p))	}
		}
			
		$gain
		{
			{0.0}		@type "exp_in"	
			0.05  {1.0}	@type "exp_in"
			($dur-0.15)   {0.9}	@type "exp_in"
			0.1  {0.0}	
		}
	}
}

;whenever ($PITCH)
;{
;	@wahwah($PITCH, $DURATION)
;}

@INSERT "zInclude/Lib_ofSil-harms2.txt"

;;;;; Level initializations
;curve wahwah-out 6.0, -96 60m
curve hr1-out 6.0, -96 60ms
curve hr2-out 6.0, -96 60ms
curve hr3-out 6.0, -96 60ms
curve hr4-out 6.0, -96 60ms
curve hr-out-db-ctrl 6.0, 3.0 100ms

	print Initialization ends

;$tab := [1, 2, 3]
;print (@resize($tab, 5))

NOTE 4900 0.333 b1
;	 sax->wah 0.0 
NOTE 5100 0.666 
NOTE 4900 0.666 

	   
NOTE 5100 0.666 b2
	$pedal := ::pedal(4900)


NOTE 5200 0.333 
NOTE 4900 0.666 
NOTE 5200 0.333 
	   
NOTE 5100 0.666 b3
NOTE 5500 0.333 
NOTE 4900 0.666 
NOTE 5200 0.333 
	   
NOTE 5100 0.333 b4
	   
NOTE 5500 0.666 
NOTE 5200 0.333 
NOTE 5500 0.333 
NOTE 5700 0.666 
	   
NOTE 5100 0.333 b5+
NOTE 4900 0.333 
NOTE 5200 0.666 
NOTE 5500 0.333 
	   
NOTE 5100 0.333 b6
NOTE 5700 0.666 
NOTE 5500 0.333 
NOTE 4900 0.666 
NOTE 5100 0.666 b7
	   
NOTE 5200 0.333 
NOTE 4900 0.666 
NOTE 5200 0.666 
	   
NOTE 4900 0.333 b8+
NOTE 5100 0.666 
NOTE 5700 0.666 
	   
NOTE 5500 0.333 b9
NOTE 4900 0.666 
NOTE 5200 0.333 
NOTE 5100 0.666 
	   
NOTE 5200 0.333 b10
NOTE 5500 0.666 
NOTE 5700 0.333 
NOTE 5100 0.666 
	   
NOTE 4900 0.333 b11
NOTE 5500 0.666 
NOTE 5200 0.333    
NOTE 5100 0.333 
	   
NOTE 0 0.333 
NOTE 4900 0.333 b12 
	 ;harm pitch 4900  
	 ;harm->timeeB 0.0  
	 ;sax->harm 0.0  
NOTE 5100 0.666 
	   
NOTE 4900 0.333 
	   
NOTE 5200 0.666 

	$pedal.$localPitch := 5200

	; harm pitch 5200 
 
NOTE 5100 0.333 b13
	   
NOTE 5500 0.666 
	   
NOTE 5700 0.333 
	   
NOTE 5200 0.666 
	   
NOTE 4900 0.333 b14
	   
NOTE 5100 0.666 
	   
NOTE 5200 0.333 
	   
NOTE 4900 0.666 
	   
NOTE 5200 0.333 b15
	   
NOTE 5100 0.333 
	   
NOTE 0 0.333 
NOTE 5500 0.333 
	   
	$pedal.$localPitch := 5500


	; harm pitch 5500  
NOTE 4900 0.666 
	   
NOTE 5200 0.333 b16
	   
NOTE 5100 0.333 
	   
NOTE 5500 0.333 
	   
NOTE 5700 0.333 
	   
NOTE 5100 0.666 
	   
NOTE 4900 0.333 b17
	   
	$pedal.$localPitch := 4900

	; harm pitch 4900  
NOTE 5100 0.333 
	   
NOTE 5200 0.333 
	   
NOTE 5100 0.333 
	   
NOTE 5700 0.666 
	   
NOTE 5800 0.333 b18
	   
NOTE 5700 0.333 
	   
NOTE 5500 0.333 
	   
NOTE 5200 0.333 
	   
NOTE 0 0.666 
	abort $pedal

	print pedal abort
	; harm pitch off  
	 ;sax->harm -127  
	 ;harm->timeeB -127  
NOTE 5700 0.333 b19
	   
NOTE 5500 0.333 
	   
NOTE 5200 0.333 
	   
NOTE 5100 0.333 
	   
NOTE 0 0.666 
NOTE 5500 0.333 b20
	   
NOTE 5200 0.333 
	   
NOTE 5100 0.333 
	   
NOTE 4900 0.333 
	   
NOTE 0 1.0 
NOTE 5800 0.333 b21+
	   
NOTE 5700 0.333 
	   
NOTE 5100 0.333 
	   
NOTE 0 1.332 
NOTE 5800 0.333 b22+
	   
NOTE 5200 0.333 
	   
NOTE 0 1.332 
NOTE 5800 0.333 b23+
	   
NOTE 5700 0.333 

	   
;VARIANCE 0.3
NOTE 0 0.666 
NOTE 7600 4.0 b24+25

; Start d2d on b. 26 
;	 1.6 d2d->timeeD s2_25.aif -6.0 
	
/*svpscrub 0
ScrubPos 0.
scrubbuf s2_25.aif -3.0
 
@hp1(9,0.0)
2.0 svpscrub 1		; start playing
curve SVP  @Grain := 0.05 , @Action := ScrubPos $x ,@target{b26+}
{
	$x
	{	
	 { 0.0  } ;IBMk	
	 3.0 { 1313.0 } 
	 (1757.-1313.)ms { 1757.07 } ;IBMk
	}
}	*/

	 
	 
NOTE 0 0.666 b26 
NOTE 5800 0.333 b26+
;	abort wah_b24
	   
NOTE 5900 0.333 
	   
NOTE 0 1.332 
NOTE 5200 0.333 b27+
	   
NOTE 5100 0.333 
	   
NOTE 5500 0.333 
	   
NOTE 0 1.0 
NOTE 6100 0.333 b28+
	   
NOTE 5900 0.333 
	   
NOTE 5800 0.333 
	   
NOTE 0 1.0 
NOTE 5900 0.333 b29+
	   
NOTE 5500 0.333 
	   
NOTE 5800 1.0 
	   
NOTE 0 0.333 b30+ 
NOTE 5200 0.333 
	   
NOTE 5100 0.333 
	   
NOTE 5700 1.0
	   
NOTE 5500 0.333 b31+
	   
NOTE 5700 0.333 
	   
NOTE 5800 0.333 
	   
NOTE 5900 1.0 
	   
NOTE 6100 0.666 b32+
	   
NOTE 5900 0.333 
	   
NOTE 5500 1.0 fltz
	   
NOTE 5200 1.0 b33+
	   
NOTE 5100 1.0
	   
NOTE 5700 1.0 b34+
;svpscrub 0
;ScrubPos 0
	   
NOTE 5800 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 0 0.666 b35 
NOTE 6100 1.0 no_fltz

NOTE 5900 0.333 

NOTE 5800 0.666 b36
NOTE 0 0.333 
NOTE 6100 1.0 

NOTE 6300 0.333 b37


/*scrubbuf s2_37.aif -3.0
 
@hp1(9,0.0)

svpscrub 1		; start playing
curve SVP  @Grain := 0.05 , @Action := ScrubPos $x, @target{b38_sync}
{
	$x
	{	
	 { 0.0  } ;IBMk	
	 3.0 { 1252.0 } 
	 (1758.-1252.)ms { 1758 } ;IBMk
	}
}	

whenever ($x=1252.0)
{
	abort SVP
	curve SVP @Grain := 0.05s, @Action := ScrubPos $x
	{
		$x
		{
			{ 1252.0 } 
			(1758.-1252.)ms { 1758 }
		}
	}
}*/

	   
	 ;sax->harm 0.0  
	 ;harm pitch 6300  
	 ;harm->timeeB 0.0 

	$pedal := ::pedal(6300)

 
NOTE 5700 2/3 
	   
NOTE 5800 1/3 
	   
NOTE 5900 1/3 
	   
NOTE 6100 1.0
	   
NOTE 6300 1/3 b38+
	   
NOTE 6400 2/3 

	   
NOTE 6500 1/3  b38_sync 
	   
NOTE 6400 0.333 b39
	   
NOTE 6500 0.333 
	   
	abort $pedal
	 ;sax->harm -127  
	 ;harm pitch off  
	 ;harm->timeeB -127  
NOTE 0 0.666 
NOTE 5500 0.333 
	   
NOTE 6100 0.666 fltz
	   
NOTE 5800 0.333 b40+
	   
NOTE 5900 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 0 0.333 
NOTE 6500 0.333 b41
	   
NOTE 6300 0.666 
	   
NOTE 6400 0.333 
	   
NOTE 6100 0.333 fltz
	   
NOTE 5900 0.333 
	   
NOTE 6400 0.333 b42
	   
NOTE 5800 0.333 
	   
NOTE 5700 0.333 
	   
NOTE 5500 0.666 
	   
NOTE 6500 0.333 fltz
	   
NOTE 6400 0.333 b43
	   
NOTE 6300 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 5900 0.333 
	   
NOTE 5800 0.333 
	   
NOTE 5700 0.666 
	   
NOTE 6700 0.333 b44+_no_fltz
	   
NOTE 7000 0.333 
	   

	$pedal := ::pedal(6300)

;	 sax->harm 0.0  
;	 harm pitch 7000  
;	 harm->timeeB 0.0  
NOTE 6700 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 6400 0.333 
	   
NOTE 6100 0.333 b45
	   
NOTE 6400 0.333 
	   
NOTE 0 0.333 
NOTE 5900 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 6300 0.333 
	   
NOTE 6400 0.333 b46
	   
NOTE 6500 0.333 
	   
NOTE 6700 0.333 
	   

NOTE 7000 0.333 
	 d2d->timeeE s2_46.aif -3.0  
	 
	 /*
	 The goal is to create: trj 1: lat dip <-> vert dip 
	 
	 EcoordW 0.71  ; puts to CARDIO on X Y Z = 0 0 1
	 Etraj 0 0 360  0 0 -360 3.0 
	 
	 Etraj has the following arguments:
	 3 x 1/2 degree step X Y Z values for departure
	 3 x 1/2 degree step X Y Z values for arrival 
	 + oscillation duration in beats	 
	 */
	 /*loop srcE 3.0		; E is source #5
	 {
	 	 @LateralDipole(5, 1.5)
	 	 1.5 @Vertical(5, 1.5)
	 }*/
	 
NOTE 6700 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 5900 0.333 b47
	   
NOTE 6100 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 6700 0.333 
	   
NOTE 7000 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 7600 5.0 b48_50
	 ;d2d->timeeD s2_48.aif -6.0  
	 ;harm->timeeB -127  

	 ;harm->timeeC 0.0  
	 ;sax->harm 0.0  
	 ;harm pitch 7900 

	$pedal.$localPitch := 7900

NOTE 0 1.0 
	;	abort wah_b48_50
	 ;print Silence after b48_50 @local 
	 ;sax->harm -127	500  


	 ;harm->timeeC -127 500 
	 ;harm->wah -127	500 
NOTE 5100 0.666 b51
	   
	 ;harm pitch 5100  
	 ;sax->harm 0.0  
	 ;harm->timeeB 0.0  

	$pedal.$localPitch := 5100

NOTE 5200 0.333 
	   
NOTE 5500 0.333 
	   
NOTE 5700 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 6500 0.333 b52
	   
NOTE 6400 0.333 
	   
NOTE 6300 0.666 
	   
NOTE 6500 0.333 
	 ;d2d->timeeF s2_52.aif 0.0  
	   
	 ;harm pitch 6500  

	$pedal.$localPitch := 6500

NOTE 6400 0.333 
	   
NOTE 6100 0.333 b53
	   
NOTE 6700 0.333 
	   
NOTE 7000 0.333 
	   
NOTE 6700 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 6400 0.666 
	   
NOTE 6100 0.333 b54+
	   
NOTE 6300 0.333 
	   
NOTE 5700 0.333 
	   
NOTE 5800 0.333 
	 ;d2d->timeeI s2_54.aif -3.0  
	 
$pedal.$localPitch := 5800
	
;	 harm pitch 5800  
NOTE 5900 0.333 
	   
NOTE 5800 0.333 b55
	   
NOTE 5700 0.666 
	   
NOTE 6400 0.666 
	   
NOTE 6300 0.333 
	   
NOTE 6400 0.333 b56
	   
NOTE 6700 0.333 
	   
NOTE 7000 0.666 
	   
NOTE 5700 0.666 
	 ;d2d->timeeF s2_56.aif 0.0  
	   
	 ;harm pitch 5700  

	$pedal.$localPitch := 6500

NOTE 7000 0.666 b57
	   
NOTE 6700 0.333 
	   
NOTE 6500 0.666 
	   
NOTE 6400 0.666 
	   
NOTE 6500 0.666 b58+
	   
NOTE 7100 0.333 
	 ;d2d->timeeD s2_58.aif -10.0  
	   
NOTE 0 0.666 
	 ;harm pitch off  
	 ;sax->harm -127  
	 ;harm->timeeB -127  
	 ;harm pitch off 

	abort $pedal
 
NOTE 6500 0.666 b59
	   
NOTE 7000 0.666 
	   
NOTE 6400 0.666 
	   
NOTE 0 0.333 b60 
NOTE 4900 0.333 
	   
NOTE 0 0.333 
NOTE 6500 0.666 
	   
NOTE 5200 0.666 
	   
NOTE 0 0.333 b61 
NOTE 7900 0.333 
	 
NOTE 6500 0.333 
	   
NOTE 6400 0.333 
	   
NOTE 0 0.333 
NOTE 6100 0.333 b62
	   
NOTE 0 0.333 

NOTE 7900 0.333 

NOTE 6500 0.333 
	   
NOTE 6300 0.666 
	   
NOTE 6400 0.333 b63+
	   
NOTE 0 0.333 
NOTE 7900 0.333 

	 
NOTE 6500 0.666 
	   
NOTE 6400 0.666 b64
	   
NOTE 6300 0.333 
	   
NOTE 5800 0.333 
	   
NOTE 0 0.666 
NOTE 7900 0.333 
	   
NOTE 6500 0.333 b65
	   
NOTE 6400 0.666 
	   
NOTE 6500 0.333 
	   
NOTE 6100 1.0
	   
NOTE 6500 0.333 b66+
	   
NOTE 6400 0.666 
	   
NOTE 6500 0.333 
	   
NOTE 7100 1.333 
	   
NOTE 0 0.333 b67++ 
NOTE 8200 0.333 
	   
NOTE 7300 0.333 
	   
	 ;harm pitch 7300  
	 ;sax->harm -3.0  
	 ;harm->timeeC 0.0  

	$pedal := ::pedal(7300)

NOTE 7600 3.0 b68->70

NOTE 7500 0.333 b71
	;abort wah_b68-70
	   
NOTE 7400 0.333 
	   
NOTE 7200 0.333 
	   
NOTE 7100 0.333 
	   
NOTE 7000 0.333 
	   
NOTE 6700 0.333 
	   
	abort $pedal
	; harm pitch off  
	; sax->harm -127  
	; harm->timeeC -127  
NOTE 7300 0.333 b72
	   
NOTE 0 1.666 
NOTE 7900 0.333 b73
	   
NOTE 6300 0.666 
	   
NOTE 6100 0.333 
	   
NOTE 0 1.0
NOTE 7900 0.333 b74+
	   
NOTE 6300 0.666 
	   
NOTE 6500 0.333 
	   
NOTE 0 0.333 
NOTE 7900 0.333 b75
	   
NOTE 7100 0.333 
	   
NOTE 7000 0.333 
	   
NOTE 6400 0.666 
	   
NOTE 6500 0.333 
	   
NOTE 0 0.333 b76 
NOTE 7900 0.333 b76
	   
NOTE 6400 0.333 
	   
NOTE 6300 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 7000 0.333 b77
	   
NOTE 0 0.333 
NOTE 7900 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 6300 0.333 
	   
NOTE 6400 0.333 
	   
NOTE 6500 0.333 b78
	   
NOTE 5900 0.333 
	   
NOTE 0 0.333 
NOTE 7900 0.333 
	   
NOTE 6300 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 5900 0.333 b79
	   
NOTE 5800 0.333 
	   
NOTE 5600 0.333 
	   
NOTE 0 0.333 
NOTE 5800 0.333 
	   
NOTE 5900 0.333 b80
	   
NOTE 6000 0.333 
	   
NOTE 6100 0.333 
	   
NOTE 7500 8.0 b80+>84+

	curve hr1-out 0.0 60ms
	::trans([-220.0], 0.0)	
	;harm pitch off  
	;harm trans -220 0.0  
	;harm->timeeC 0.0  
	;sax->harm 0.0  
NOTE 7400 0.333 b84+
	   
	 ;harm trans -200 0.0
	 ::trans([-200], 0.0)
NOTE 7300 0.333 
	   
NOTE 0 0.333 
NOTE 7900 10.0 b85->89
	 ;d2d->timeeD s2_85.aif -15.0 		; level was 0.0 
	 ;harm->timeeC -6.0  
	 ;harm trans -233 200 0.0  

	curve hr2-out 0.0 60ms
	::trans([-233, -200], 0.0)

NOTE 7800 0.333 b90
	 ;harm->wah -127 300 
	   
	 ;harm trans -200 200 0.0 
	::trans([-200, -200], 0.0)

 
NOTE 7700 0.333 
	   
	abort ::trans
	 ;harm trans 0 0 0.0  
	 ;harm->timeeC -127  
	 ;sax->harm -127  
NOTE 0 1.666 
NOTE 8100 0.333 b91+
	   
NOTE 7000 0.333 
	   
NOTE 7100 0.666 
	   
NOTE 6500 0.333 b92
	   
NOTE 8100 0.333 
	   
NOTE 0 0.666 
NOTE 8200 0.333 
	   
NOTE 7100 0.333 
	   
NOTE 7300 0.666 b93
	   
NOTE 7100 0.333 
	   
NOTE 6700 0.666 
	   
NOTE 6500 0.333 
	   
NOTE 6400 0.333 b94
	   
NOTE 0 0.333 
NOTE 5100 0.333 
	   
NOTE 7300 0.666 
	   
NOTE 7200 0.333 b95
	   
NOTE 7100 0.666 
	   
NOTE 7000 0.333 
	   
NOTE 6700 0.666 
	   
NOTE 0 0.333 b96 
NOTE 8100 0.333 b96
	   
NOTE 7500 0.333 
	   
NOTE 7300 0.333 
	   
NOTE 7200 0.333 
	   
NOTE 7000 0.666 
	   
NOTE 7200 0.1667 b97+
	   
NOTE 7400 0.1667 
	   
NOTE 0 1.0 
NOTE 8100 0.333 
	   
NOTE 7600 0.333 b98
	   
NOTE 7500 0.333 
	   
NOTE 0 0.666 
NOTE 8200 0.333 
	   
NOTE 7100 0.333 
	   
NOTE 7500 0.666 
	   
NOTE 0 0.333 
NOTE 8200 0.333 
	   
NOTE 6700 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 0 0.666 b100 
NOTE 8200 0.333 b100
	   
NOTE 6400 0.333 
	   
NOTE 0 0.333 
NOTE 4900 0.333 
	   
NOTE 6500 0.333 b101
	   
NOTE 6400 0.333 
	   
NOTE 6300 0.333 
	   
NOTE 0 0.666 
NOTE 8200 0.333 
	   
NOTE 7100 0.333 b102
	   
NOTE 0 0.333 
NOTE 4900 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 6400 0.333 
	   
NOTE 0 1.0
NOTE 8300 0.333 b103+
	   
NOTE 6700 0.333 
	   
NOTE 0 0.333 
NOTE 8300 0.333 
	   
NOTE 6300 0.333 b104
	   
NOTE 6400 0.333 
	   
NOTE 0 0.666 
NOTE 4900 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 6400 0.666 b105
	   
NOTE 8600 0.333 
	   
NOTE 7500 0.333 
	   
NOTE 7300 0.333 
	   
NOTE 0 0.666 
NOTE 4900 0.333 b106+
	   
NOTE 6500 0.333 
	   
NOTE 6400 0.666 
	   
NOTE 6300 0.333 
	   
NOTE 6400 0.333 b107
	   
NOTE 6500 0.666 
	   
NOTE 0 0.333 
NOTE 4900 0.333 
	   
NOTE 6400 0.333 
	   
NOTE 6500 0.666 b108
	   

NOTE 8300 0.333 

	   
NOTE 7000 0.333 
	   
TRILL ( 6700 6800 ) 1.0
NOTE 7000 0.1667 b109+
	   
NOTE 7100 0.1667 b109+
	   
NOTE 0 0.333 
NOTE 8100 0.333 
	   
NOTE 0 0.333 
NOTE 7100 0.333 
	   
NOTE 7000 0.333 b110
	   
TRILL ( 6700 6800 ) 0.8327 
NOTE 7000 0.1667 
	   

NOTE 0 0.333 
NOTE 8100 0.333 
	 d2d->timeeF s2_110.aif -3.0  
	   
TRILL ( 6700 6800 ) 1.333 b111 
NOTE 4900 0.333 
	   
NOTE 6700 1.0
	   
NOTE 6600 0.333 b112+
	   
NOTE 6500 0.333 
	   
TRILL ( 6400 6300 ) 1.332 
NOTE 4900 0.333 b113+
	   
NOTE 6200 0.333 
	   
NOTE 6300 0.333 
	   
NOTE 6400 0.666 
	   
NOTE 6300 0.333 b114+
	   
NOTE 6400 0.333 
	   
;     PFWD 0.25 d2d->timeeG s2_114.aif -9.0 
TRILL ( 6500 6700 ) 1.666 
	 d2d->timeeG s2_114.aif -9.0  
NOTE 6600 0.1667 b115+
	   
NOTE 6500 0.1667 b115+
	   
NOTE 6400 0.333 
	   
;     PFWD 0.25 d2d->timeeI s2_115.aif -10.0 
TRILL ( 7000 6700 ) 1.666 
	 d2d->timeeI s2_115.aif -10.0  
NOTE 7100 0.333 b116+
	   
NOTE 0 0.666 
NOTE 7300 0.333 b117
	   
NOTE 7200 0.333 
	   
NOTE 7100 0.333 
	   
TRILL ( 7000 7600 ) 2.0 
NOTE 7500 0.333 b118+
	   
NOTE 7400 0.333 
	   
NOTE 7300 0.333 
	   
NOTE 7200 0.333 b119
	   
NOTE 7100 0.333 
	   
NOTE 7000 0.333 
	   
TRILL ( 7900 7600 ) 1.666 
NOTE 6300 0.333 b120+
	   
	 d2d->timeeH s2_120.aif -6.0  

	 ;HcoordW 0.71  
	 ;Htraj 0 0 360  0 0 -360  2.0  
NOTE 6400 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 6600 0.333 
	   
NOTE 6700 0.333 b121
	   
TRILL ( 7000 7600 ) 1.332 
NOTE 8300 0.333 
	   
TRILL ( 7000 6700 ) 1.666 b122 
NOTE 6500 0.333 
	   
NOTE 6400 0.333 b123
	   
NOTE 6300 0.333 
	   
NOTE 6200 0.333 
	   
NOTE 6100 0.333 
	   
TRILL ( 5900 6100 ) 2.332 
NOTE 7000 0.333 b124++
	   
NOTE 0 1.333 
NOTE 6500 0.333 b125+
	   
NOTE 6100 0.333 
	   
NOTE 6700 0.333 
	   
NOTE 7100 0.333 b126
	   
NOTE 7000 0.333 
	   
NOTE 7600 5.666 b126-129
     
	 harm trans 520 313 0.0  
	 harm->timeeC -6.0  
	 sax->harm 0.0  
NOTE 0 0.333 b129+ 
	 harm->timeeC -127  
	 sax->harm -127  
	 harm trans 0 0 0.0  
NOTE 6300 0.333 b129+
	   
NOTE 5700 0.666 
	   
NOTE 7000 0.333 
	   
NOTE 0 0.333 b130 
NOTE 7000 0.333 b130
	   
NOTE 8300 0.333 
	   
NOTE 7600 0.333 
	   
NOTE 7500 0.333 
	   
NOTE 7400 0.333 
	   
NOTE 7900 6.25 
	 harm trans 210 -635 0.0  
	 harm->timeeC -6.0  
	 sax->harm 0.0  
	 harm->wah -6.0 

NOTE 7800 0.25 b134+
	 harm->wah -127.0 
	   
	 harm trans 0 0 0.0  
	 harm->timeeC -127  
	 sax->harm -127  
NOTE 7700 0.25 
	   
NOTE 7600 0.25 
	   
NOTE 7500 0.25 
	   
NOTE 7400 0.25 
	   
NOTE 7300 0.25 
	   
NOTE 7200 0.25 
	   
NOTE 0 0.333 b135 
NOTE 6100 0.333 b135
	   
NOTE 6300 0.333 
	   
NOTE 8100 0.333 
	   
NOTE 0 0.333 
NOTE 7000 0.1667 
	   
NOTE 7100 0.1667 
	   
NOTE 7200 0.1667 b136
	   
NOTE 7300 0.1667 
	   
NOTE 0 0.333 
NOTE 8100 0.333 
	   
NOTE 7000 0.333 
	   
NOTE 6700 0.333 
	   
NOTE 8100 7.666 
	 harm trans -190 -420 -641 0.0  
	 harm->timeeC -9.0  
	 sax->harm 0.0  
 
NOTE 0 0.333 b140- 
	 harm->wah -127.0 
	 harm trans 0 0 0 0.0  
	 harm->timeeC -127  
	 sax->harm -127  
NOTE 6700 0.333 b140
	   
NOTE 5800 0.666 b141
	   
NOTE 7500 0.666 
	   
NOTE 7900 0.333 
	 

	 ;HcoordW 0.71  
	 ;Htraj 0 0 360  0 0 -360  2.0  
NOTE 6500 0.333 
	   
NOTE 0 0.333 
NOTE 5100 0.333 
	   
NOTE 6500 0.333 
	   
NOTE 6400 0.333 
	   
NOTE 8200 9.0 b143-147

      
	 harm trans 0 -80 -317 -880 -1133 0.0  
	 harm->timeeC -10.0  
	 sax->harm 0.0  
NOTE 0 0.666 b147+ 
	 harm->wah -127 
NOTE 6900 0.333 b147+
	   
NOTE 5900 0.666 
	   
NOTE 4900 0.666 b148
	   
NOTE 6400 0.333 
	   
NOTE 7500 0.333 
	   
NOTE 0 0.333 
NOTE 7400 0.333 
	   
NOTE 7300 0.333 b149
	   
NOTE 0 0.333 
NOTE 7000 0.333 
	   
NOTE 7600 13.333 b149-156
;	 d2d->timeeE s2_149.aif -5.0 		; level was 0.0 
	   
	 harm trans 0 0 0 0 0 0.0, 710 593 -292 -413 11.0   
	 harm->timeeC -10.0  
	 sax->harm 0.0 

	 ;HcoordW 0.71  
	 ;Htraj 0 0 360  0 0 -360  10.0  

/*		abort srcH
	 loop srcH 10.0		; H is source #8
	 {
	 	 @LateralDipole(8, 5.0)
	 	 5.0 @Vertical(8, 5.0)
	 }


	 harm->wah -10.0 
	group wah_b149
	{
		 @wahwah(0.33)
		0.33  @wahwah(1.0)
		1.0  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(1.0)
		1.0  @wahwah(0.66)
		0.66  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(1.0)
		1.0  @wahwah(1.0)
		1.0  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
	}  */
NOTE 7500 0.1667 b156+
	   
	 harm trans 0 0 0 0 0.0  
	 harm->timeeC -127  
	 sax->harm -127  
NOTE 7400 0.1667 
	   
NOTE 7300 0.333 
	   
NOTE 0 0.333 
NOTE 7900 0.333 
	   
NOTE 7300 0.333 b157
	   
NOTE 0 0.333 
NOTE 7200 0.333 
	   
NOTE 7900 0.333 
	   
NOTE 0 1.332 
NOTE 7200 0.333 b158+
	   
NOTE 7100 0.666 
	   
NOTE 7900 0.666 
	   
NOTE 7100 0.333 b159+
	   
NOTE 7000 0.333 
	   
NOTE 0 0.333 
NOTE 6900 0.333 
	   
NOTE 7900 0.333 
	   
NOTE 0 0.333 b160 
NOTE 6900 0.333 b160
	   
NOTE 6800 0.333 
	   
NOTE 0 0.333 
NOTE 7900 0.333 
	   
NOTE 0 1.0
NOTE 6800 0.333 b161
	   
NOTE 6700 0.666 
	   
NOTE 6600 0.666 
	   
NOTE 6500 0.333 b162+
	   
NOTE 8100 0.333 
	   
NOTE 0 0.333 
NOTE 6500 0.333 
	   
NOTE 6400 0.333 
	   
NOTE 0 0.333 b163 

NOTE 8100 0.333 b163
	   
NOTE 6400 0.333 
	   
NOTE 6300 0.333 
	   
NOTE 6200 0.333 
	   
NOTE 0 0.666 
NOTE 8100 0.333 b164+
	   
NOTE 6200 0.666 
	   
NOTE 6100 0.666 
	   
NOTE 0 0.666 b165 
NOTE 7900 0.333 b165
	   
NOTE 6100 0.666 
	   
NOTE 7900 0.333 
	   
NOTE 6000 0.666 b166
	   
NOTE 8100 0.333 
	   
NOTE 6000 0.666 
	   
NOTE 8100 0.333 
	   
NOTE 6000 0.333 b167
	   
NOTE 5900 0.333 
	   
NOTE 8100 0.333 
	   
NOTE 0 1.333 

NOTE 8200 0.333 b168+
	   
NOTE 6100 0.333 
	   
NOTE 5900 1.0 
	   
NOTE 5700 1.0 b169
	   
NOTE 8200 0.333 
	   
NOTE 5700 0.666 
	   
NOTE 8200 0.333 b170
	   
NOTE 5700 0.333 
	   
NOTE 5600 0.333 
	   
NOTE 5500 0.333 
	   
NOTE 8200 0.333 
	   
NOTE 5500 0.333 
	   
NOTE 5400 0.333 b171
	   
NOTE 5300 0.666 
	   
NOTE 8200 0.333 
	   
NOTE 0 0.333 
NOTE 5300 0.666 
	   
NOTE 5200 0.666 			b172
	   
NOTE 0 0.333 
NOTE 8300 0.333 
	   
NOTE 5200 0.333 
	   
NOTE 5100 0.666 			b173
	   
NOTE 5000 0.333 
	   
TRILL ( 4900 5500 ) 1.666 b173+>174 
;NOTE 5500 0.333 b174+
;	   
NOTE 7100 0.666 
	   
NOTE 7000 0.333 
	   
NOTE 0 0.333 b175 
NOTE 8500 0.333 
	   
NOTE 7100 0.666 
	   
NOTE 7000 0.1667 
	   
NOTE 6900 0.1667 
	   
NOTE 6800 0.1667 
	   
;     PFWD 0.56 d2d->timeeE s2_176.aif -6.0 
;     PFWD d2d->timeeG s2_176a.aif -10.0 
;     PFWD d2d->timeeF s2_176b.aif -6.0 
NOTE 0 0.5 
NOTE 8300 14.333 b176-182
	 d2d->timeeE s2_176.aif -6.0  
	 d2d->timeeG s2_176a.aif -10.0  
	 d2d->timeeF s2_176b.aif -6.0  
	 harm trans 0 0 0 0 0 0 0.0, -1 -207 -392 -721 -1030 -1341 12.0  
	 harm->timeeC -10.0  
	 sax->harm 0.0  

	; HcoordW 0.71  
	; Htraj 0 0 360  0 0 -360  10.0

/*	 harm->wah -10.0 
	group wah_b176
	{
		 @wahwah(0.33)
		0.33  @wahwah(0.66)
		0.66  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(1.0)
		1.0  @wahwah(0.33)
		0.33  @wahwah(0.66)
		0.66  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(1.0)
		1.0  @wahwah(0.33)
		0.33  @wahwah(0.66)
		0.66  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.66)
		0.66  @wahwah(0.33)
		0.33  @wahwah(0.66)
		0.66  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.66)
		0.66  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
	}  */
NOTE 7900 0.333 b183+
	 harm->wah -127.0 
	   
	 harm trans -203 0 0 0 0 0 0.0  
	 harm->timeeC -3.0  
	 sax->harm 0.0  
NOTE 8100 0.333 
	   
;     PFWD 0.285 d2d->timeeD s2_183.aif -9.0 
NOTE 8500 0.666 
	 d2d->timeeD s2_183.aif -9.0  
	   
NOTE 8300 0.333 
	   
NOTE 8200 1.0 
	   
;     PFWD 0.92 d2d->timeeI s2_184.aif -3.0 
NOTE 8500 0.333 
	 d2d->timeeI s2_184.aif -3.0  
	   
NOTE 7100 0.333 
	   
NOTE 7000 0.333 
	   
;     PFWD 0.92 d2d->timeeF s2_185.aif -5.0 
NOTE 0 0.666 b185 
NOTE 8500 0.333 b185
	 d2d->timeeF s2_185.aif -5.0  
	   
	 harm->timeeC -6.0 
	 harm trans -207 -312 0 0 0 0 0.0  
NOTE 7500 0.666 
	   
NOTE 7300 0.333 
	   
;     PFWD 0.285 d2d->timeeG s2_186.aif -9.0 
NOTE 8300 0.333 b186
	 d2d->timeeG s2_186.aif -9.0  
	   
NOTE 7000 0.333 
	   
NOTE 0 0.666 
NOTE 6700 0.333 
	   
NOTE 8300 0.333 
	   
NOTE 7000 0.666 b187
	   
NOTE 8300 0.333 
	   
NOTE 7000 0.333 
	   
	 harm trans -202 -307 148 0 0 0 0.0  

TRILL ( 6700 6800 ) 1.332 
	 d2d->timeeD s2_187.aif -10.0  
NOTE 4900 0.333 b188+
	   
NOTE 6700 0.333 
	   

TRILL ( 7000 7100 ) 1.333 
	 d2d->timeeI s2_188.aif -5.0  
NOTE 8300 0.333 b189+
	   
NOTE 0 0.333 
NOTE 7000 0.1667 
	   
	 harm->timeeC -10.0 
	 harm trans -193 -317 152 361 0 0 0.0  
NOTE 7300 0.1667 
	   
	 d2d->timeeF s2_189.aif 0.0  
TRILL ( 7500 7600 ) 2.0 
	 harm trans -193 -317 152 361 0 0 0.0  @local 
NOTE 7600 0.333 b190+
	   
NOTE 4900 0.333 
	   
	 harm trans -33 -75 82 121 193 -194 0.0  
NOTE 6500 0.333 
	   
NOTE 7600 0.333 b191
	   
NOTE 7500 0.333 
	   
TRILL ( 7300 7500 ) 6.666 "b191-194" 
	/*loop wah_trill1 0.1 
	{
		 @wahwah(0.1)
	}*/

NOTE 8600 3.333 
	/*KILL wah_trill1
	group wah_8600
	{
		 @wahwah(0.33)
		0.33  @wahwah(0.66)
		0.66  @wahwah(0.66)
		0.66  @wahwah(0.66)
		0.66  @wahwah(0.33)   
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)
		0.33  @wahwah(0.33)  
	}	*/
	 harm pitch 8600  
	 3.333 d2d->timeeG s2_194.aif -3.0 			; Fermata 
MULTI ( 8600 8500 8400 8200 ) 1.0 b195 
;-------- IMPORTANT: All WAHs stop here!

NOTE 0 1.0 196 
	 harm pitch off  
	 harm trans 0 0 0 0 0 0 0.0  
; Turn off waa-waa 
NOTE 7300 0.666 b195+
NOTE 7100 0.666  
NOTE 5100 0.333 b197+
NOTE 6400 0.333 
NOTE 0 0.333 
NOTE 5100 0.333 
NOTE 6400 0.333 
NOTE 0 0.333 b198 
NOTE 7000 0.333 b198+
NOTE 8300 2.6660 
	;loop wah_198 0.33 
	;{
;		 @wahwah(0.33)
;	}
NOTE 7000 0.333 "b197+"
	KILL wah_198
NOTE 6700 0.333 
NOTE 0 0.666 b200 
NOTE 7100 0.1667 "200+"
	 d2d->timeeG s2_203.aif -5.0 
NOTE 7000 0.1667 
NOTE 6700 0.333 
NOTE 5100 0.333 
NOTE 6400 0.333 
NOTE 6500 0.333 b201
NOTE 6700 0.333 
NOTE 7000 0.333 
NOTE 7100 0.333 
NOTE 7300 0.333 
NOTE 7500 0.333 
NOTE 7600 0.333 b202
	 d2d->timeeG s2_202_1.aiff -10.0 
	 0.33 d2d->timeeG s2_202_2.aiff -10.0 
NOTE 7900 0.333 
NOTE 8100 0.333  
NOTE 8200 0.333 
NOTE 8300 0.333 
NOTE 8400 0.333 
TEMPO OFF
NOTE 8500 6.0 s2-lastharm
;	loop wah_203 0.33 
;	{
;		 @wahwah(0.33)
;	}
	 harm trans -66 -134 -210 -290 -410 -533 -652 -781 0.0  
	 harm->timeeB -9.0  
	 harm->timeeC -9.0  
	 sax->harm 0.0 2000  
NOTE 8100 1/8 
NOTE 7500 1/8 
NOTE 7000 1/8
NOTE 6400 1/8
NOTE 5700 1/8
NOTE 5100 1/8
NOTE 5000 1/8 s2-end 
	 d2d->timeeC s2_204_1.aiff -10.0 
	 d2d->timeeA s2_204_2.aiff -10.0 
	 KILL wah_203
	 Etraj off 
	 Htraj off 
	 harm trans 0 0 0 0 0 0 0 0 0.0  
	 harm->timeeB -127  
	 harm->timeeC -127  
	 sax->harm -127  
; Disabling input to antescofo for queing by hand 
	 antescofo-in 0  
; Loading next score: section 3 
	 0.5s antescofo-score ofSil_3.txt  
