/* Transposition Antescofo processes and Macros for
	... of Silence by Marco Stroppa
	Library of methods
	written by Arshia Cont, August 2014
*/

;;;;;;;;; Definition of global variables for Harmonizers
; Table for storing harmonizer values. Last element is $harmvalues[5] 
$harmvalues := [0.0 | (10)]	

;; Convenience macros for each harmonizer
@macro_def harm($num,$x)
{
	;;; Set local level value and shut off if zero
	if ($x=0.0)
	{
		@command("hr"+$num+"-out") -120
	}else
	{
		@command("hr"+$num+"-out") 0.0	
	}
	;;; Set harm value
	@command("hr"+$num+"-p") (@pow(2., ( $x/100.0)/12.0))
	$harmvalues[($num-1)] := $x
}

/* Pedal Process
		Argumment: $PedalPitch in midicent value (int or float)
	NOTE: Additionally, you can change the pedal pitch while the process is active by changing
			the value of $processInstance.$localPitch
*/ 
@PROC_DEF pedal($PedalPitch)	
	@abort:= 
		{	
			curve hr1-out -96 500ms 
			500ms @harm(1, 0.0); zero transposition is equal to ONE pitch factor!
		}
{
	; turn the specific harm on
	curve hr1-out 0.0 60ms
	
	@local $localPitch
	$localPitch := $PedalPitch

	@harm(1, ($localPitch - $PITCH) )
	whenever ($PITCH)
	{
		@harm(1, ($localPitch - $PITCH) )
		;hr1-p (@pow(2., (($localPitch - $PITCH)/100.0)/12.0))
		;$harmvalues[0] := (@pow(2., (($localPitch - $PITCH)/100.0)/12.0))
	}

	whenever ($localPitch)
	{
		@harm(1, ($localPitch - $PITCH) )
		print Local Pitch changed to $localPitch on pitch $PITCH
	}
}

/* Trans process
 *		Arguments:
 *			$transtab: table consisting of destination values.
 *			$dur:	duration for interpolation
 * Note: Always departs from last value of the harmonizer table $harmvals
*		 Aborting the process will set all values back to 0.0 after fadeout
 *
*/
@PROC_DEF trans($transp, $dur)
	@abort :=
	{
		@harm(1,0.0)
		@harm(2,0.0)
		@harm(3,0.0)
		@harm(4,0.0)
		@harm(5,0.0)
		@harm(6,0.0)
		@harm(7,0.0)
		@harm(8,0.0)
	}
{
	;;; prepare destination
	; if input table does not have size of global harms, then convert it so by adding zeros
	; This is also the place to turn harmonizers ON/OFF automatically if need be.
	@local $transdest, $zerovec
	if ( (@size($transp)) = @size($harmvalues))
	{
		$transdest := $transp
	}else		; 
	{
		; size o transtab should be smaller than $harmvalues.
		; concatenate a zeroed vector
		$zerovec := [ 0.0 | ( (@size($harmvalues)-@size($transp)) ) ]
		$transdest := @concat($transp, $zerovec)
	}

	print DEBUG: Trans request on ($transp)
	;;; launch curve interpolation
	curve trans @grain := 0.03s,  
		@action :=
		{
			@harm(1,$x1)
			@harm(2,$x2)
			@harm(3,$x3)
			@harm(4,$x4)
			@harm(5,$x5)
			@harm(6,$x6)
			@harm(7,$x7)
			@harm(8,$x8)
		}
	{
		$x1, $x2, $x3, $x4, $x5, $x6, $x7, $x8
		{
			{ ($harmvalues[0]) ($harmvalues[1]) ($harmvalues[2])  ($harmvalues[3]) ($harmvalues[4]) ($harmvalues[5]) ($harmvalues[6])  ($harmvalues[7]) }
			$dur { ($transdest[0]) ($transdest[1]) ($transdest[2]) ($transdest[3]) ($transdest[4]) ($transdest[5]) ($transdest[6]) ($transdest[7])  }
		}
	}
}
