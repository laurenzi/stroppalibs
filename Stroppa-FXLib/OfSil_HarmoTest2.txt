BPM 60


;;;;;;;;; Load Library
@INSERT "zInclude/Lib_Stroppa-harms3.txt"



;;;;;;;;;;;; INITIALIZATIONS
; global level control for all harms in DB (post)
curve hr-out-db-ctrl -96, 0. 300ms	
; individual harm level controls in DB (post). Needs to be set before hr-out-db-ctrl
curve hr1-out -96, -96 300ms
curve hr2-out -96, -96 300ms
curve hr3-out -96, -96 300ms
curve hr4-out -96, -96 300ms

fs1_fre 0.0
fs2_fre 0.0
fs3_fre 0.0
fs4_fre 0.0
curve fs1-out -96, -96 100ms	; individual fs level in DB
curve fs2-out -96, -96 100ms
curve fs3-out -96, -96 100ms
curve fs4-out -96, -96 100ms
curve fs-out-db-ctrl -96, -0. 100ms		; global post level fs in DB

; wah wah initialization
wah-Q 5.0
wah-filter gainresonant
curve wahwah-out -96, -96 300ms


NOTE C4 5.0

curve hr1-out  -6. 300ms
curve hr2-out  -3. 300ms
curve hr3-out  -6. 300ms
curve hr4-out  -4. 300ms
; hr-out-db-ctrl controls harm levels in DB
curve hr-out-db-ctrl -6.0 500ms	



NOTE D4 5.0
curve hr-out-db-ctrl 0.0 500ms	 
curve hr1-out  0. 300ms
loop hr1-baland 2.0	@abort:= {  curve hr1-out  -96. 60ms }
{
	curve slider  @Grain := 0.05s, @Action := 
		{
			@harm(1, $hr1)
		}
	{
		$hr1
		{
			{ -1. } /*@type "exp"*/
		1   { 1. }
		1	{ -1. }
		}
	}
}


NOTE E4 5.0	SingleHarm-Jitter

curve hr-out-db-ctrl 0.0 500ms	 
curve hr1-out  0. 300ms
loop hr1-jitter 200ms	@abort:= {  curve hr1-out  -96. 60ms }
{
	curve slider  @Grain := 0.01s, @Action := 
		{
			@harm(1, $hr1)
		}
	{
		$hr1
		{
			{ -0.25 } /*@type "exp"*/
		100ms   { 0.25 }
		100ms	{ -0.25 }
		}
	}
}

abort hr1-jitter

curve hr1-out  0. 300ms
	$random1 := @random()
loop hr1-jitter2 200ms	@abort:= {  curve hr1-out  -96. 60ms }
{
	@local $temp
	curve slider  @Grain := 0.01s, 
		@Action := 
		{
			$temp := $hr1
			@harm(1, $hr1)
		}
	{
		$hr1
		{
			{ (-0.5*$random1) } /*@type "exp"*/
		100ms   { (0.5*@random()) }
		100ms	{ (-0.5*@random()) }
		}
	}
	200ms $random1 := $temp
}

abort hr1-jitter2

NOTE F4 5.0 Trans

curve hr1-out  0. 300ms
curve hr2-out  0. 300ms
::trans( [-200, 200], 2.0)

curve hr3-out  0. 300ms
::trans( [-100, 100, -50], 5.0)

curve hr4-out  0. 300ms
::trans( [-100, 100, -50, 50], 2.0)
 
NOTE G4 5.0 Trans-Reordering

; Aborting ::trans will cut off all harms! So you'd need to reinitiate their levels!
abort ::trans
;;; IMPORTANT NOTE: The above is NOT effective if ::trans has already finished (auto-kill) like a Group


curve hr1-out  0. 300ms
curve hr2-out  0. 300ms
curve hr3-out  0. 300ms
curve hr4-out  0. 300ms

;;; Scrambling (random re-ordering)
$transpositions := [ -75, -25, 25, 75]
$loop := 2.0

loop scramble $loop
{
	::trans($transpositions, $loop)
	$loop $transpositions := @scramble($transpositions)
}

abort scramble

;;; Permuting the same list in a loop
$transpositions := [ -75, -25, 25, 75]
$loop := 2.0

loop permutation $loop
{
	::trans($transpositions, $loop)
	$loop $transpositions := @permute($transpositions, (@rand_int(@size($transpositions))))
}

abort permutation

;;; Order reversal
$transpositions := [ -75, -25, 25, 75]
$loop := 2.0

loop reversal $loop
{
	::trans($transpositions, $loop)
	$loop $transpositions := @reverse($transpositions)
}

abort reversal

;;; Rotation
$transpositions := [ -75, -25, 25, 75]
$loop := 2.0

loop rotate $loop
{
	::trans($transpositions, $loop)
	$loop $transpositions := @rotate($transpositions, 1)	; use negative for left rotation
}

abort rotate


NOTE C4 5.0	fs-Jitter

curve fs-out-db-ctrl 0.0 500ms	 
curve fs1-out 0. 100ms
loop fs1-jitter 100ms	@abort:= {  curve fs1-out  -96. 60ms }
{
	curve slider  @Grain := 0.005s, @Action := fs1_fre $f1
	{
		$f1
		{
			{ -50. } /*@type "exp"*/
		50ms   { 50 }
		50ms	{ -50 }
		}
	}
}

abort fs1-jitter

NOTE F4 5.0 wahwah

/*@wahwah(6900, 3.0)
@wahwah(6900, 1.0)

@wahwah(5800, 3.0)
@wahwah(5800, 1.0)

group wah
{
	@wahwah(5800, 1.0)
	0.5 @wahwah(5800, 1.0)
	0.5 @wahwah(5800, 1.0)
	0.5 @wahwah(5800, 1.0)
}

group wah
{
	@wahwah(6900, 1.0)
	1.0 @wahwah(6900, 1.0)
	0.5 @wahwah(6900, 1.0)
	0.5 @wahwah(6900, 1.0)
}
*/
