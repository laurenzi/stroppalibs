{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 9,
			"architecture" : "x86"
		}
,
		"rect" : [ 25.0, 69.0, 640.0, 480.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 10.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial Bold",
		"gridonopen" : 0,
		"gridsize" : [ 10.0, 10.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 310.0, 190.0, 101.0, 18.0 ],
					"text" : "DelayControl 4 dell"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 310.0, 130.0, 98.0, 18.0 ],
					"text" : "DelayControl 8 del"
				}

			}
 ],
		"lines" : [  ],
		"dependency_cache" : [ 			{
				"name" : "DelayControl.maxpat",
				"bootpath" : "/Users/laurenzi/Documents/PRODUCTION/Prod anciennes/Stroppa_/Auras_2015",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "auras.delv.02~.maxpat",
				"bootpath" : "/Users/laurenzi/Documents/PRODUCTION/Prod anciennes/Stroppa_/Auras_2015/lib/Arshia",
				"patcherrelativepath" : "./lib/Arshia",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bD.pat",
				"bootpath" : "/Users/laurenzi/Documents/PRODUCTION/Prod anciennes/Stroppa_/Auras_2015/lib/chamber-lib",
				"patcherrelativepath" : "./lib/chamber-lib",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "parMod2",
				"bootpath" : "/Users/laurenzi/Documents/PRODUCTION/Prod anciennes/Stroppa_/Auras_2015/lib/chamber-lib",
				"patcherrelativepath" : "./lib/chamber-lib",
				"type" : "maxb",
				"implicit" : 1
			}
, 			{
				"name" : "DelayControl.js",
				"bootpath" : "/Users/laurenzi/Documents/PRODUCTION/Prod anciennes/Stroppa_/Auras_2015",
				"patcherrelativepath" : ".",
				"type" : "TEXT",
				"implicit" : 1
			}
 ]
	}

}
