; ============================== STROPPA Delay LIBRARY ==================================== (12-10-2015)
;  
; last updated (10-10-2015)

; ================================================================================================

/* NOTES:
	Things to be defined for this Library:
	
	An Antescofo map called $delconfig that contains presets for Delay parameters (del, fbk, gain, panmin, panmax, panmode)
	It shoulb be defined by user.
	Ranges : del (0-1), fbk (dB), gain (dB), pan (0-100), panmin (0-100), panmax (0-100), panmode (static, random, sine, triangle)
	
	befinition example:      
	$delconfig := map { 
	( "b01", [0.1, -100, -3, 10, 0, 100, 0]),	
	( "b02", [0.2, -90, -3, 10, 0, 100, 0]),
	( "b03", [0.3, -80, -3, 20, 0, 100, 0]),
	( "b04", [0.4, -70, -6, 20, 0, 100, 0]),
	( "b05", [0.5, -60, -6, 20, 0, 100, 0]), 
	( "b06", [0.6, -80, -6, 80, 0, 100, 0]),
	( "b07", [0.7, -70, -6, 80, 0, 100, 0]),
	( "b08", [0.8, -60, -9, 80, 0, 100, 0])	
	}	
	
	$$delconfig can be defined AFTER the @INSERT command of the library.


	A map of presets for all Delays called $dicoPreset   // Un bico be preset : on associe un tableau be config à un nom be presets
	befinition example: 
	$dicoPreset := map {
	("Delpres1", [ "a01", "a01", "a01", "a04", "a05", "a06", "a07", "a08", a09"]),
	("Delpres2", [ "b01", "b01", "b01", "b01", "b01", "b01", "b01", "b01", b09"])
	}


*/
; ================================================================================================

;  default Dictionary of configurations
; define $delconfig here in case it is undefined. User definition SHOULD come next (after @include)

  
if (@is_undef($delconfig))  
{
	print DELAY Preset Library : delconfig was set to empty
	$delconfig := map { }  // still no set of configurations defined
} 


; default number of mobules
$number_of_modules := 10


if (@is_undef($dicoPreset))
{
	print "Dictionary of presets was set to empty"
	$dicoPreset := map {}
}


if (@is_undef($deltab))
{
 	print DEL TAB: deltab was set to empty
 	$deltab := []

} 
 
; ----------------------------------- INITIALIZATION  ----------------------------------------------


; init default namespace
$GlobalDelsNamespace := "toto"


; Initialize and (re)construct $source structure
@proc_def ::InitDelays($NumberOfDels, $DelNameSpace, $mastdel, $mastFbk)
{
	
	; (re)define namespace and number of mobules  
	$GlobalDelsNamespace := $DelNameSpace
	$number_of_modules := $NumberOfDels

	print "The Delay namespace" ($GlobalDelsNamespace) "has dimension reset with" ($number_of_modules) "Delay units"
	

	; default master Delay and master feedBack values
	@command($GlobalDelsNamespace+"-mstdel") $mastdel ;
	@command($GlobalDelsNamespace+"-mstfbk") $mastFbk ;


    $t := tab [$x | $x in $NumberOfDels]
    forall $x in $t
    {
    	@command($GlobalDelsNamespace+($x+1)+"-del")  0;
		@command($GlobalDelsNamespace+($x+1)+"-amp")  -127;
		@command($GlobalDelsNamespace+($x+1)+"-fbk")  -3;
		@command($GlobalDelsNamespace+($x+1)+"-pan")  0;
		@command($GlobalDelsNamespace+($x+1)+"-panmin")   0;
		@command($GlobalDelsNamespace+($x+1)+"-panmax")  100;
		@command($GlobalDelsNamespace+($x+1)+"-panmode")  0;
    }

     @command($GlobalDelsNamespace+"_out_gain") -150.0 	;; turns off global gain (linear)

}

@global $initState 
@global $numdelay

; set params for a SINGLE Delay UNIT
@proc_def ::setDelayState($numdelay, $initState)
{
    	@command($GlobalDelsNamespace+($numdelay)+"-del")  (($delconfig($initState))[0]) ;
		@command($GlobalDelsNamespace+($numdelay)+"-amp")  (($delconfig($initState))[1]);
		@command($GlobalDelsNamespace+($numdelay)+"-fbk")  (($delconfig($initState))[2]);
		@command($GlobalDelsNamespace+($numdelay)+"-pan")  (($delconfig($initState))[3]);
		@command($GlobalDelsNamespace+($numdelay)+"-panmin")  (($delconfig($initState))[4]);
		@command($GlobalDelsNamespace+($numdelay)+"-panmax")  (($delconfig($initState))[5]);
		@command($GlobalDelsNamespace+($numdelay)+"-panmode")  (($delconfig($initState))[6]);
}

; set params for ALL Delay UNITS
@proc_def ::setAllDelayState($deltab)
{
	if (@is_tab($deltab))
	{
	    if (@size($deltab) != $number_of_modules)
	    {
	    	print "ERROR: incorrect numbers of mobules in ::setAllDelayState: got" (@size($deltab)) "expected:" ($number_of_modules) 
	    }
	    else
	    {
			forall $i in @size($deltab)
			{
				::setDelayState(($i+1), $deltab[$i])
			}
		}
	}
	else
	{
		print "ERROR: :setAllDelayState expect a tab as argument, got" ($deltab)
	}
}


; set params for SOME Delay UNITS  ;;  argument is a table with pairs of elements :  [[inbex, etat] ... ]
@proc_def ::setSomeDelayState($delconfig)
{
	forall $p in $delconfig
	{
	    if ($p[0] < 0 || $p[0] >= $number_of_modules)
    	{
    		print "ERROR: incorrect modules index in ::setSomeDelayState got" ($p[0]) "max expected:" ($number_of_modules) 
    	}
    	else
    	{
		
			::setDelayState($p[0], $p[1])
		}
	}
}

;;; 	 ::setPreset("DelPres2", 0.4, -3)
; set a PRESET of PARMATERS for ALL DelayS 
@macro_def @setPreset($x, $mastdel, $mastFbk) 
{ 
	;; is_member CHECK
	::setAllDelayState($dicoPreset($x)) 

	@command($GlobalDelsNamespace+"-mstdel") $mastdel ;
	@command($GlobalDelsNamespace+"-mstfbk") $mastFbk ;
	
	print ($dicoPreset($x))
}


/*
; possibilities for the INITIALIZATION of ALL MObULES
;
;    ::setAllDelayState([ "b01", "b01", "b01", "b04", "b05", "b06", "b07", "b08"])
;
;    // alternativement en stockant la config bans une variable
;    $deltab := tab [ "b01", "b02", "b03", "b04", "b05", "b06", "b07", "b08"]
;    ; ::setAllDelayState( $deltab)
;
;    // alternativement en utilisant un preset
;    ::setAllDelayState( $dicoPreset("preset2") )
;    @setPreset("preset2") ; équivalent
;
;    // config que be certains mobules
;    ::setSomeDelayState( [ [3, "b01"], [4, "b08"], [6, "b05"] ] )
;
; définition bes config par béfaut pour cette partition 
*/


/* 
;; EXAMPLES for the Synthaxe ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 


NOTE 60 1. event01

::InitDelays(8, "del", 0.11, -33.)


NOTE 60 1. event02
::setDelayState(2, "b05")

NOTE 60 1. event03
::setSomeDelayState([ [2, "b03"], [3,"b04"], [6,"b03"] ])

NOTE 60 1. event04
::setAllDelayState(["b05", "b04", "b03", "b02", "b01", "b06", "b08", "b07"])

NOTE 60 1. event05
::setSomeDelayState([ [2, "b05"], [3,"b04"], [6,"b03"] ])

NOTE 60 1. event06
@setPreset("Preset2", 0.2, -6)

*/

;NOTE 70 1 event06

; ::displaydicopresets( "Delpres1" ) 
; ::displaypresets( "a05")