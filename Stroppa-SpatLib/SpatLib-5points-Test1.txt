BPM 60

@INSERT "zInclude/Lib_Stroppa-Spat8.txt"

;;; Initialize sources: syntax is ::InitSources($NumberOfSources, $NumberOfSpeakers)
::InitSources(10, 5, "src")

$initlevels := map {
	("FadeOut", [0., 0., 0., 0., 0.]),
	
; SINGLE LOUDSPEAKERS
	("Front", [1., 0., 0., 0., 0.]),
	("Left", [0., 1., 0., 0., 0.]),
	("Rear", [0., 0., 1., 0., 0.]),
	("Right", [0., 0., 0., 1., 0. ]),
	("Vert", [0., 0., 0., 0., 1.]),

; DOUBLE LOUDSPEAKERS, WITH PHASE OPPOSITION
	("FrontalDipole", [.7, 0., -0.7, 0., 0.]),
	("LateralDipole", [0., .7, 0., -0.7, 0.]),

	("FrontLeftCorner", [0.7, -0.7, 0., 0., 0.]),
	("RearLeftCorner", [0., 0.7, -0.7, 0., 0.]),
	("RearRightCorner", [0., 0., .7, -0.7, 0.]),
	("FrontRightCorner", [-0.7, 0., 0., 0.7, 0.]),

	("FrontVertCorner", [-0.7, 0., 0., 0., 0.7]),
	("LeftVertCorner", [0., -0.7, 0., 0., 0.7]),
	("RearVertCorner", [0., 0., -0.7, 0., 0.7]),
	("RightVertCorner", [0., 0., 0., -0.7, 0.7]),

; DOUBLE LOUDSPEAKERS, IN PHASE
	("FrontalDipole1", [0.6, 0., 0.6, 0., 0.]),
	("LateralDipole1", [0., 0.5, 0., 0.5, 0.]),

	("FrontLeftCorner1", [0.7, 0.7, 0., 0., 0.]),
	("RearLeftCorner1", [0., 0.7, 0.7, 0., 0.]),
	("RearRightCorner1", [0., 0., 0.7, 0.7, 0.]),
	("FrontRightCorner1", [0.7, 0., 0., 0.7, 0.]),

	("FrontVertCorner1", [0.7, 0., 0., 0., 0.7]),
	("LeftVertCorner1", [0., 0.7, 0., 0., 0.7]),
	("RearVertCorner1", [0., 0., 0.7, 0., 0.7]),
	("RightVertCorner1", [0., 0., 0., 0.7, 0.7]),

; TRIPLE LOUDSPEAKERS, OUT OF PHASE
	("FrontTriple", 	[0.65, -0.35, 0., 0.35, 0.]	),
	("FrontTriple01", 	[0.65, 0.35, 0., -0.35, 0.]),
	("FrontTriple02", 	[-0.65, .35, 0., 0.35, 0.]),
	("LeftTriple", 		[0.35, 0.65, -0.35, 0., 0.]),
	("RearTriple", 		[0., 0.35, 0.65, -0.35, 0.]),
	("RightTriple", 	[-0.35, 0., 0.35, 0.65, 0.]),

; TRIPLE LOUDSPEAKERS, IN PHASE
	("FrontTriple1", [0.65, 0.35, 0., 0.35, 0.]),
	("LeftTriple1", [0.35, 0.65, 0.35, 0., 0.]),
	("RearTriple1", [0., 0.35, .65, 0.35, 0.]),
	("RightTriple1", [0.35, 0., 0.35, 0.65, 0.]),

; ALL LOUDSPEAKERS
	("AllHorizontal", [.45, -.45, .45, -.45, 0.]),
	("AllDipoles", [.45, .45, -.45, -.45, 0.]),
	("AllHorizontalinPhase", [.45, .45, .45, .45, 0.]),

	("AllVertical", [.4, .4, -.4, -.4, .4]),
	("AllinPhase", [.4, .4, .4, .4, .4])
}

@FadeOut(1, 0.)
@FadeOut(2, 0)




; ------------------------------------------------------------------------------------------

NOTE G4 4.0 randomspace_5s

;;; Calling random space
$randomspace1 := ::RandSpace_i(1 , ["Front", "Left", "Right", "Vert", "Rear"] ,0.05, 0.1, 0.5, 5.0, "sine_in_out")


;**********************************************************************
; BALANCE OF INDIVIDUAL LOUDSPEAKERS (NORMALLY IT SHOULD BE SET UP AT THE MIXING BOARD)
; Four horizontal loudspeakers
NOTE C2 4.0 Lsp_hor
print "BALANCE OF INDIVIDUAL LOUDSPEAKERS (NORMALLY IT SHOULD BE SET UP AT THE MIXING BOARD)"
print "Four horizontal loudspeakers"
loop Lsp_hor 4.0 @tempo := 90.0
{
	@Space(1, "Front",  0.5)
	1.0 @Space(1, "Left",  0.5)
	1.0 @Space(1, "Rear",  0.5)
	1.0 @Space(1, "Right",  0.5)
}
NOTE C#2 4.0 Lsp_hor_stop
abort Lsp_hor
@FadeOut(1, 0.1)


; all the loudspeakers
NOTE D2 5.0 Lsp_all
print "All the loudspeakers"
loop Lsp_all 5.0 @tempo := 90.0
{
	@Space(1, "Front",  0.5)
	1.0 @Space(1, "Left",  0.5)
	1.0 @Space(1, "Rear",  0.5)
	1.0 @Space(1, "Right",  0.5)
	1.0 @Space(1, "Vert",  0.5)
}
NOTE D#2 5.0 Lsp_all_stop
abort Lsp_all
@FadeOut(1, 0.1)


; alternance horizontal-vertical
NOTE F2 5.0 Lsp_hor-vert
print "Alternance horizontal-vertical"
loop Lsp_hor-vert 8.0 @tempo := 90.0
{
	@Space(1, "Front",  0.5)
	1.0 @Space(1, "Vert",  0.5)
	1.0 @Space(1, "Left",  0.5)
	1.0 @Space(1, "Vert",  0.5)
	1.0 @Space(1, "Rear",  0.5)
	1.0 @Space(1, "Vert",  0.5)
	1.0 @Space(1, "Right",  0.5)
	1.0 @Space(1, "Vert",  0.5)
}
NOTE F#2 5.0 Lsp_hor-vert_stop
abort Lsp_hor-vert
@FadeOut(1, 0.1)


;**********************************************************************
; BALANCE SINGLE LOUDSPEAKER / DIPOLE
; NB: avoid using the same single loudspeaker as in a dipole
; Frontal Dipole
NOTE C3 5.0 FrontDipole ; front / rear
print "BALANCE SINGLE LOUDSPEAKER / DIPOLE"
print "Frontal Dipole"
loop FrontDipole 6.0 @tempo := 90.0
{
	@Space(1, "FrontalDipole",  0.5)
	1.0 @Space(1, "Left",  0.5)
	1.0 @Space(1, "FrontalDipole",  0.5)
	1.0 @Space(1, "Right",  0.5)
	1.0 @Space(1, "FrontalDipole",  0.5)
	1.0 @Space(1, "Vert",  0.5)
}
NOTE C#3 5.0 FrontDipole_stop
abort FrontDipole
@FadeOut(1, 0.1)


; Lateral Dipole
NOTE D3 5.0 LateralDipole ; front / rear
print "Lateral Dipole"
loop LateralDipole 6.0 @tempo := 90.0
{
	@Space(1, "LateralDipole",  0.5)
	1.0 @Space(1, "Front",  0.5)
	1.0 @Space(1, "LateralDipole",  0.5)
	1.0 @Space(1, "Rear",  1.0)
	1.0 @Space(1, "LateralDipole",  0.5)
	1.0 @Space(1, "Vert",  0.5)
}
NOTE D#3 5.0 LateralDipole_stop
abort LateralDipole
@FadeOut(1, 0.1)


;______________________________________________________________________
; BALANCE DIPOLES
; BALANCE IN-PHASE / OUT-OF-PHASE DIPOLES
; not really impressive!
NOTE F3 5.0 InOutDipoles ; front / rear
print "BALANCE DIPOLES"
print "BALANCE IN-PHASE / OUT-OF-PHASE DIPOLES"
loop InOutDipoles 4.0 @tempo := 60.0
{
	@Space(1, "FrontalDipole",  0.5)
	1.0 @Space(1, "FrontalDipole1",  0.5)
	1.0 @Space(1, "LateralDipole",  0.5)
	1.0 @Space(1, "LateralDipole1",  0.5)
}
NOTE F#3 5.0 InOutDipoles_stop
abort InOutDipoles
@FadeOut(1, 0.1)

;**********************************************************************
; BALANCE SINGLE LOUDSPEAKER / CORNERS
; NB: avoid using the same single loudspeaker as in a dipole
; Horizontal Corners
NOTE C4 4.0 Hor_Corners
print "BALANCE SINGLE LOUDSPEAKER / CORNERS"
print "Horizontal Corners"
loop Hor_Corners 8.0 @tempo := 90.0
{
	@Space(1, "FrontLeftCorner",  0.5)
	1.0 @Space(1, "Right",  0.5)
	1.0 @Space(1, "RearLeftCorner",  0.5)
	1.0 @Space(1, "Front",  0.5)
	1.0 @Space(1, "RearRightCorner",  0.5)
	1.0 @Space(1, "Left",  0.5)
	1.0 @Space(1, "FrontRightCorner",  0.5)
	1.0 @Space(1, "Rear",  0.5)
}
NOTE C#4 4.0 Hor_Corners_stop
abort Hor_Corners
@FadeOut(1, 0.1)


; Vertical Corners
NOTE D4 4.0 Vert_Corners
print "Vertical Corners"
loop Vert_Corners 8.0 @tempo := 90.0
{
	@Space(1, "FrontVertCorner",  0.5)
	1.0 @Space(1, "Right",  0.5)
	1.0 @Space(1, "LeftVertCorner",  0.5)
	1.0 @Space(1, "Front",  0.5)
	1.0 @Space(1, "RearVertCorner",  0.5)
	1.0 @Space(1, "Left",  0.5)
	1.0 @Space(1, "RightVertCorner",  0.5)
	1.0 @Space(1, "Rear",  0.5)
}
NOTE D#4 4.0 Vert_Corners_stop
abort Vert_Corners
@FadeOut(1, 0.1)


;______________________________________________________________________
; BALANCE CORNERS
; BALANCE IN-PHASE / OUT-OF-PHASE CORNERS
; horizontal
NOTE F4 5.0 InOutCorners ; front / rear
print "BALANCE CORNERS"
print "BALANCE IN-PHASE / OUT-OF-PHASE CORNERS"
print "Horizontal"
loop InOutCorners 8.0 @tempo := 60.0
{
	@Space(1, "FrontLeftCorner",  0.5)
	1.0 @Space(1, "FrontLeftCorner1",  0.5)
	1.0 @Space(1, "RearLeftCorner",  0.5)
	1.0 @Space(1, "RearLeftCorner1",  0.5)
	1.0 @Space(1, "RearRightCorner",  0.5)
	1.0 @Space(1, "RearRightCorner1",  0.5)
	1.0 @Space(1, "FrontRightCorner",  0.5)
	1.0 @Space(1, "FrontRightCorner1",  0.5)
}
NOTE F#4 5.0 InOutCorners_stop
abort InOutCorners
@FadeOut(1, 0.1)


; vertical
NOTE G4 5.0 InOutVertCorners
print "Vertical"
loop InOutVertCorners 8.0 @tempo := 60.0
{
	@Space(1, "FrontVertCorner",  0.5)
	1.0 @Space(1, "FrontVertCorner1",  0.5)
	1.0 @Space(1, "LeftVertCorner",  0.5)
	1.0 @Space(1, "LeftVertCorner1",  0.5)
	1.0 @Space(1, "RearVertCorner",  0.5)
	1.0 @Space(1, "RearVertCorner1",  0.5)
	1.0 @Space(1, "RightVertCorner",  0.5)
	1.0 @Space(1, "RightVertCorner1",  0.5)
}
NOTE G#4 5.0 InOutVertCorners_stop
abort InOutVertCorners
@FadeOut(1, 0.1)


;**********************************************************************
; BALANCE SINGLE LOUDSPEAKER / TRIPLE CONFIGURATIONS
; NB: avoid using the same single loudspeaker as in a dipole
; FrontTriple
NOTE C5 5.0 SingleFront ; PB?
print "BALANCE SINGLE LOUDSPEAKER / TRIPLE CONFIGURATIONS"
print "FrontTriple"
loop SingleFront 2.0 @tempo := 60.0
{
	@Space(1, "FrontTriple",  0.5)
	1.0 @Space(1, "Rear",  0.5)
}
NOTE C#5 5.0 SingleFront_stop
abort SingleFront
@FadeOut(1, 0.1)


NOTE D5 5.0 SingleLeft ; PB?
print "BALANCE SINGLE LOUDSPEAKER / TRIPLE CONFIGURATIONS"
print "LeftTriple"
loop SingleLeft 2.0 @tempo := 60.0
{
	@Space(1, "LeftTriple",  0.5)
	1.0 @Space(1, "Right",  0.5)
}
NOTE D#5 5.0 SingleLeft_stop
abort SingleLeft
@FadeOut(1, 0.1)


NOTE F5 5.0 SingleRear ; front / rear
print "BALANCE SINGLE LOUDSPEAKER / TRIPLE CONFIGURATIONS"
print "RearTriple"
loop SingleRear 2.0 @tempo := 60.0
{
	@Space(1, "RearTriple",  0.5)
	1.0 @Space(1, "Front",  0.5)
}
NOTE F#5 5.0 SingleRear_stop
abort SingleRear
@FadeOut(1, 0.1)


NOTE G5 5.0 SingleRight ; front / rear
print "BALANCE SINGLE LOUDSPEAKER / TRIPLE CONFIGURATIONS"
print "RightTriple"
loop SingleRight 2.0 @tempo := 60.0
{
	@Space(1, "RightTriple",  0.5)
	1.0 @Space(1, "Left",  0.5)
}
NOTE G#5 5.0 SingleRight_stop
abort SingleRight
@FadeOut(1, 0.1)


;______________________________________________________________________
; BALANCE TRIPLE
; BALANCE IN-PHASE / OUT-OF-PHASE TRIPLE
NOTE A5 5.0 InOutTriple ; front / rear
print "BALANCE IN-PHASE / OUT-OF-PHASE TRIPLE CONFIGURATIONS"
print "InOutFrontTriple"
loop InOutFrontTriple 5.0 @tempo := 60.0
{
	@Space(1, "FrontTriple",  0.5)
	1.0 @Space(1, "FrontTriple1",  0.5)
	1.0 @Space(1, "FrontTriple",  0.5)
	1.0 @Space(1, "FrontTriple01",  0.5)
	1.0 @Space(1, "FrontTriple02",  0.5)
}
NOTE A#5 5.0 InOutFrontTriple_stop
abort InOutFrontTriple
@FadeOut(1, 0.1)


NOTE B5 5.0 InOutTriple ; front / rear
print "BALANCE IN-PHASE / OUT-OF-PHASE TRIPLE CONFIGURATIONS"
print "InOutAllTriple"
loop InOutAllTriple 8.0 @tempo := 60.0
{
	@Space(1, "FrontTriple",  0.5)
	1.0 @Space(1, "FrontTriple1",  0.5)
	1.0 @Space(1, "LeftTriple",  0.5)
	1.0 @Space(1, "LeftTriple1",  0.5)
	1.0 @Space(1, "RearTriple",  0.5)
	1.0 @Space(1, "RearTriple1",  0.5)
	1.0 @Space(1, "RightTriple",  0.5)
	1.0 @Space(1, "RightTriple1",  0.5)
}
NOTE Bb5 5.0 InOutAllTriple_stop
abort InOutAllTriple
@FadeOut(1, 0.1)



;______________________________________________________________________
; BALANCE SINGLE LOUDSPEAKER / QUADRUPLE CONFIGURATIONS
; AllHorizontal / Vertical
NOTE C6 5.0 AllHorizontal ; front / rear
print "BALANCE SINGLE LOUDSPEAKER / QUADRUPLE CONFIGURATIONS"
print "AllHorizontal / Vertical"
loop AllHorizontal 6.0 @tempo := 60.0
{
	@Space(1, "AllHorizontal",  0.5)
	1.0 @Space(1, "Vert",  0.5)
	1.0 @Space(1, "AllDipoles",  0.5)
	1.0 @Space(1, "Vert",  0.5)
	1.0 @Space(1, "AllHorizontalinPhase",  0.5)
	1.0 @Space(1, "Vert",  0.5)
}
NOTE C#6 5.0 AllHorizontal_stop
abort AllHorizontal
@FadeOut(1, 0.1)


;______________________________________________________________________
; BALANCE SINGLE LOUDSPEAKER / ALL
; All vs. Single
NOTE D6 5.0 AllSingle
print "BALANCE SINGLE LOUDSPEAKER / ALL"
print "All / Single"
loop AllSingle 10.0 @tempo := 60.0
{
	@Space(1, "AllVertical",  0.5)
	1.0 @Space(1, "Front",  0.5)
	1.0 @Space(1, "AllVertical",  0.5)
	1.0 @Space(1, "Left",  0.5)
	1.0 @Space(1, "AllVertical",  0.5)
	1.0 @Space(1, "Rear",  0.5)
	1.0 @Space(1, "AllVertical",  0.5)
	1.0 @Space(1, "Right",  0.5)
	1.0 @Space(1, "AllVertical",  0.5)
	1.0 @Space(1, "Vert",  0.5)
}
NOTE D#6 5.0 AllSingle_stop
abort AllSingle
@FadeOut(1, 0.1)


NOTE F6 5.0 All
print "BALANCE ALL LOUDSPEAKERS"
print "All in/out Phase"
loop All 2.0 @tempo := 60.0
{
	@Space(1, "AllVertical",  0.5)
	1.0 @Space(1, "AllinPhase",  0.5)
}
NOTE F#6 5.0 All_stop
abort All
@FadeOut(1, 0.1)

;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; END OF TESTS
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




