; SPATIAL LIBRARY FOR STROPPA ANTESCOFO/MAX LIBRARIES
; Last update: 26/02/2016

/* NOTES:
	This library uses an Antescofo map called $initlevels that contains destinations (as key strings)
	and their coordinates (as value tables).
	It should be defined by the user.
	
	Definition example:
	$initlevels := map {
	("FadeOut", [0., 0., 0., 0., 0.]),
	("Left", [0., 1., 0., 0., 0.])
	}

*/

; Initialize and (re)construct $source structure
@proc_def ::InitSources($NumberOfSources, $NumberOfSpeakers, $Namespace)
{
	abort (obj::source)
	;; Stores addresses of each object...
	$source := [0.0 | ($NumberOfSources)]	
    ;; (re)instantiate source objects and store them somewhere!
    $t := tab [$x | $x in $NumberOfSources]
    forall $x in $t
    {
    	$source[$x] := obj::source($x, $NumberOfSpeakers, $Namespace)
    }
    ;print Created @size($source) Spatial Sources with @size($source[1].$coordinates) points 
}

; define $initlevels here in case it is undefined. User definition SHOULD come next (after @include)
if (@is_undef($initlevels))
{
	print SPAT LIB: initlevels was set to empty
	$initlevels := map { }
}

; define $source here in case it is undefined. User definition SHOULD come next (after @include)
if (@is_undef($source))
{
	; defines vector of source addresses
	$source := [0.0 | (10)]		
}


; SOURCE Object Definition
@obj_def source($idn, $npoints, $namespace)
{
	; Create local variables that are specific to each instance of the object
	@local $coordinates, $idnum, $last_action, $prefix_namespace, $last_goto
	;@local $coordinates 		; This is where we store local coordinates of this source.
								; This is always a variable table whose length is equal to # of speakers
	;@local $idnum				; ID Number of the receiver in patch
	;@local $last_action         ; store the address of last action for aborts etc.
	;@local $prefix_namespace	; string used to build send-symbol for Max/pd as $prefix_namespace+$idnum+"-spat"

	;; Object @init is automatically called when the object is created
	@init 
	{
		$idnum := ($idn + 1)
		$last_action := 0
		$coordinates := [0.0 | ($npoints)]		; create a table of zeros
		;print "init first coordinates" $idnum $coordinates
		$prefix_namespace := $namespace
	; last_igoto: initialize memory of the pointer to the last curve run by igoto
	;	(to avoid superposing points at the same logical time)
		$last_goto := 0
	}

	;; A broadcasted signal to all instances of source objects. Useful in panics!
	@broadcast reset()
	{
		;; abort ongoing curves or processes
		abort $last_action
	}

	;; Defining an abort will do additional actions when this object is aborted/killed.
	;	Useful for specific terminal operations.
	@abort
	{
		print "Source " ($idnum) " is killed!"
		abort $last_action
	}

	;;;;;;;; USER-DEFINED METHODS
	;; SPACE_JUMP: immediately go to $destination and save the position that was reached
	/*
	@proc_def space_jump($destination)
	{
		$coordinates := ($initlevels($destination))
		@command($prefix_namespace+$idnum+"-spat") ($initlevels($destination))
	}
	*/

	@fun_def space_jump($destination)
	{
	    ; print space_jump $idnum " old coordinate:" $coordinates " destination:" $destination " new coord:" ($initlevels($destination))
		if (! @is_defined($initlevels, $destination))
		{
			print "==> init levels @is_undef($destination) " (@is_undef($destination))
			;print "==> init levels ! @is_defined($initlevels, $destination) " (! @is_defined($initlevels, $destination))
			;print "==> init levels NOT initialized : destination = " $destination "$initlevels=" ($initlevels($destination))
		}
		else
		{
			$coordinates := ($initlevels($destination))
			@command($prefix_namespace+$idnum+"-spat") ($initlevels($destination))
		}
	}


    @proc_def goto($dest, $dur, $stay, $itp, $mm)
    {
    	;print ". new goto from " $coordinates " to " $dest " in " $dur " with " $itp " and tempo " $mm " stay " $stay

    	; If $sdur is 0.0, use SPACE_JUMP method instead (conceptually cleaner)
		if ($dur == 0.0) 
		{
			;print "Space_jumping" ($snum-1) " in " $source
			_ := .space_jump($dest)
		}
		else 
		{
			;print Initial coordinate of source ($snum) is ($source[$snum].$coordinates)
			;$dur print Final coordinate of source ($snum) is ($source[$snum].$coordinates)

		   	@local $x
		   	@local $delay := $dur*$stay
			;print $delay
		    abort $last_goto  
		    $delay
		    $last_goto := {
		    	;print igoto $prefix_namespace $idnum
		    	;print FlyingEngine delay $delay destination $dest duration ($dur - 0.01) coordinate $coordinates destination ($initlevels($dest))
				curve FlyingEngine3 
					@tempo := ($mm == false ? $RT_TEMPO : $mm)
				    @Grain := 0.05s   
				    @exclusive
					@Action := 
					{
						;print "coordinate: " $x at $NOW
						$coordinates := $x  ; store in internal/local coordinates of this source to keep track
						@command($prefix_namespace+$idnum+"-spat") ($x)
					}
				{
					$x
					{
						{ ($coordinates) } @type $itp
						// was: (@max(0.0, $dur - $delay - 0.01))  { ($initlevels($dest)) } @type $itp
						(@max(0.01, $dur - $delay))  { ($initlevels($dest)) } @type $itp
					}
				}
			}
			; $dur print end FlyingEngine $NOW
		}
    }

	

	;; Random space local method 
	;_________________________________________________________________________________________
	/**** Random Granular Spatial Movement Process
	* Arguments:
	*	arg1 $configs: table of string configurations from $initlevels. ex: [ "Front", "Rear", "Right"]
	*	arg2 $grainrange_min:	float - minimum grain time
	*	arg3 $grainrange_max: 	float - maximum grain time
	*	arg4 $stay_percent:		float (0.to 1.) - percentage of staying on selected configuration
	** IMPORTANT: For now, we give time values in relative. Tempo is fixed inside as 60 BPM.
	**				This means that $grainrange_min 0.05 is 50ms
	****/
	/*@proc_def RandomSpace($configs, $grainrange_min, $grainrange_max, $stay_percent)
	{
		abort $last_action
		;;; Generate first randoms:
		@local $grainsize, $randomconfig
		$grainsize := @random() * ($grainrange_max - $grainrange_min) + $grainrange_min
		;print DEBUG: Initial grain set to ($grainsize)
		
		$last_action :=
		{
			Loop L	$grainsize
			{
				; Generate a random configuration.
				$randomconfig := ($configs[@rand_int(@size($configs))])
				; print "random config: " $randomconfig $NOW
				; Make the move, in duration that is (1.0 - $stay_percent). 
				;$Ticket := [$snum, $randomconfig, ($grainsize*(1.0-$stay_percent))]
				_ := $THISOBJ.goto( $randomconfig , ($grainsize *(1.0 - $stay_percent ) ) )
		
				;print "DEBUG: random space with grain" ($grainsize) "on" ($randomconfig)
				; Generate a new period just before this loop is finishes! Looks like a harakiri!
				$grainsize := @random() * ($grainrange_max - $grainrange_min) + $grainrange_min
			} 
		}
	}
*/
	@fun_def make_unique_random_config($configs)
	{
		if (@size($configs) < 2)
		{
			print "Config size too small !!!"
		}
		else
		{
			@local $randomconfig, $diff, $curcoord
			$randomconfig := $configs[@rand_int(@size($configs))]
			$curcoord := $initlevels($randomconfig)
			$diff := @reduce(@+, @abs($coordinates - $curcoord))
			if ($diff < 1)
			{ return $THISOBJ.make_unique_random_config($configs) }
			else
			{ return $randomconfig }
		}
	}


//     @proc_def goto($dest, $dur, $stay, $itp, $mm)

	@proc_def RanSpace($configs, $grainrange_min, $grainrange_max, $stay_percent, $itp)
	{
		abort $last_action
		;;; Generate first randoms:
		@local $grainsize, $randomconfig
		$grainsize := (@random() * ($grainrange_max - $grainrange_min) + $grainrange_min )
		print DEBUG RAN SPACE: Initial grain set to ($grainsize)
		
		$last_action := {
			;$randomconfig := ($configs[@rand_int(@size($configs))])
			Loop L	$grainsize
			{
				;@local $currcfg, $prevcfgs
				; Generate a random configuration.
				$randomconfig := $THISOBJ.make_unique_random_config($configs)
				;print "random config: " $idnum "=" $randomconfig at $NOW
				; Make the move, in duration that is (1.0 - $stay_percent). 
				;$Ticket := [$snum, $randomconfig, ($grainsize*(1.0-$stay_percent))]
				_ := .goto($randomconfig, $grainsize, $stay_percent, $itp, false)
		
				;print "DEBUG: random space with grain" ($grainsize) "on" ($randomconfig)
				; Generate a new period just before this loop is finishes! Looks like a harakiri!
				$grainsize := @random() * ($grainrange_max - $grainrange_min) + $grainrange_min
				;print "grainsize" $grainsize

			} 
		}
	}
}

// 

@fun_def @Space($src, $dest, $dur = 0., $stay = 0., $itp = "sine_in_out", $mm = false)
{
	;print "=================>>>>>>>>>>>>>> goto: " ($source[$src-1]) $src $dest $mm
	$source[$src-1].goto($dest, $dur, $stay, $itp, $mm)
}



;$index  := @find($table, @f1(@rand_int(@size($table))))
;print ($titti := @f1(1, 3 ))
;$randomconfig := ($table[@rand_int(@size($table))])
;print @xrandom([4, 3, 2, 1, 0], 2)
;print @find([4, 3, 2, 1, 0], @f1(@rand_int(@size([4, 3, 2, 1, 0]))))
;print @remove([4, 3, 2, 1, 0], 3)

/*
@fun_def @xxrand($table)
{
	@local $curval, $prevval, $otherval
	$curval := ($table[@rand_int(@size($table))])
	
	loop ($curval = $prevval)
	{
		for () $otherval := 
	}
}
*/
;_________________________________________________________________________________________
; General SPACE Macro to assign movements to sources with default interpolation.
; Arguments:
;	1st arg: source number (integer)
;	2nd arg: spatial configuration (string) --> must be in the $initlevels map. Must be wrapped in quotes
;	3rd arg: duration (float, or time in seconds or milliseconds: ex. 3.0 or 3.0s or 3.0ms) to get to the
;				new spatial position
/*
@macro_def Space($snum, $scfg, $sdur) 
{	
	@Space_i ($snum, $scfg, $sdur, "sine_in_out")
}
*/
/*
@macro_def SpaceR($snum, $scfgs, $sdur, $grainrange_min, $grainrange_max, $stay_percent) 
{	
	@Space_i ($snum, $scfg, $sdur, "sine_in_out")
}
*/
; SPACE_I Macro: same as SPACE with the choice of the interpolation
; Arguments:
;	1st arg: source number (integer)
;	2nd arg: spatial configuration (string) --> must be in the $initlevels map. Must be wrapped in quotes
;	3rd arg: duration (float, or time in seconds or milliseconds: ex. 3.0 or 3.0s or 3.0ms) to get to the
;				new spatial position
;	4th arg: interpolation type (default: "sine_in_out")
/*
@macro_def Space_i($snum, $scfg, $sdur, $sitp) 
{	
	; If $sdur is 0.0, use SPACE_JUMP method instead (conceptually cleaner)
	if ($sdur == 0.0)
	{
	;print "Space_jumping" ($snum-1) " in " $source
		_ := $source[($snum-1)].space_jump ($scfg)
	}
	else {
	; Call the specific GOTO method for source object $snum	
	_ := $source[($snum-1)].igoto($scfg,$sdur,$sitp)
	}
	;print Initial coordinate of source ($snum) is ($source[$snum].$coordinates)
	;$sdur print Final coordinate of source ($snum) is ($source[$snum].$coordinates)
}
*/

;_________________________________________________________________________________________


;FadeOut
@macro_def FadeOut($sourcenum, $dur) 
{ 
	_ := $source[($sourcenum-1)].goto("FadeOut",$dur)
	;abort ($source[$snum])
}

;randomspace con una durata 



@proc_def ::RandSpace($snum, $configs, $dur, $grain_min = 0.1, $grain_max = -1., $stay = 0., $itp = "sine_in_out")
@abort { abort $titi }
{
	@local $fconfig

	
	if ($configs[0].is_tab())
	{ $fconfig := [$e[1] | $e in $configs] }
	else
	{ $fconfig := $configs }

   if ($grain_max < 0) { $grain_max := $grain_min }

	@local $titi
	$titi := $source[($snum-1)].RanSpace($fconfig, $grain_min, $grain_max, $stay, $itp)
;	print $titi
	$dur abort $titi
	     print abort $titi RandSpace at $NOW
}





;----------------------------------------------------------------------
;;; TRAJECTORY READER 

/*
A Trajectory is a table [[t0, c0], [t1, c1], ... [tn, Cn]]
	where:	t = time (where the sound is supposed to be), relative to the total duration given in Traj_reader
			c = spatial configuration to be at at time "t"
Notice that if t0 > 0.0, the source will start from the currently stored position and
 move to the written position in t0 beats; if t0 is 0, it will directly jump to this position.

This function generates a set of parallel processes using the macro Space_i.
REMARK: spatial tables SHOULD have at lease TWO elements
*/

; Traj_reader args:
; src	: source ID (an integer number)
; Traj 	: table with trajectories
; dur 	: total duration of the trajectory
; itp 	: interpolation structure
@proc_def ::Traj_reader($src, $Traj, $dur, $itp)
@abort { abort $titi }
{
	@local $durscaler, $currtime
	@local $stay := 0
	@local $mm := $RT_TEMPO

	if (($Traj.last()[0] == 0) && @size($Traj) > 1)
	{ print "ERROR trajectory zero duration in multiple step trajectory" }

	; if size Traj is only 1 step, do just a jump with @Space_i
	if ( @size($Traj) == 1 )
	{
		@Space ($src, ($Traj[0, 1]), $dur, $stay, $itp, $mm)
		; print @size($Traj) "if"
	}
	else
	{
		$durscaler := $dur / $Traj.last()[0]
		$currtime := $Traj[0, 0] * $durscaler

		; Initialization: place the first element at the right time
		; If the first time is not 0.0, reach the configuration after this time
		@Space ($src, ($Traj[0, 1]), $currtime, $stay, $itp, $mm)

		; Generate a set of parallel processes, starting from the table's second point
		forall $i in (@size ($Traj) - 1)
		{
			@local $cur, $cfg, $t0, $t1, $scaled_dur
			$t0 := ($Traj[$i, 0]) * $durscaler
			$cur := $i + 1
			$t1 := ($Traj[$cur, 0]) * $durscaler
			$cfg := $Traj[$cur, 1]
			$scaled_dur := $t1 - $t0

			$t0 @Space($src, $cfg, $scaled_dur, $stay, $itp, $mm)
			;print $t0 $src $cfg $durscaler $scaled_dur $itp
		}

		; print "else"
	}
}

@proc_def ::Traj_reader_s($src, $Traj, $dur, $itp, $stay_percent)
{
	@local $durscaler, $currtime
	@local $stay := $stay_percent
	@local $mm := $RT_TEMPO

	if (($Traj.last()[0] == 0) && @size($Traj) > 1)
	{ print "ERROR trajectory zero duration in multiple step trajectory" }

	; if size Traj is only 1 step, do just a jump with @Space_i
	if ( @size($Traj) == 1 )
	{
		@Space($src, ($Traj[0, 1]), $dur, $stay, $itp, $mm)
		; print @size($Traj) "if"
	}
	else
	{
		$durscaler := $dur / $Traj.last()[0]
		$currtime := $Traj[0, 0] * $durscaler

		; Initialization: place the first element at the right time
		; If the first time is not 0.0, reach the configuration after this time
		@Space($src, ($Traj[0, 1]), $currtime, $stay, $itp, $mm)

		; Generate a set of parallel processes, starting from the table's second point
		forall $i in (@size ($Traj) - 1)
		{
			@local $cur, $cfg, $t0, $t1, $scaled_dur
			$t0 := ($Traj[$i, 0]) * $durscaler
			$cur := $i + 1
			$t1 := ($Traj[$cur, 0]) * $durscaler
			$cfg := $Traj[$cur, 1]
			$scaled_dur := $t1 - $t0

			$t0 @Space($src, $cfg, $scaled_dur, $stay, $itp, $mm)
			;print $t0 $src $cfg $scaled_dur $stay_percent $itp
		}
		; print "else"
	}
}



; reverse a table (of spatial configurations)
@fun_def @rev_table($tbl)
{
	@local $date, $cfg, $res
	$date := [$x[0] | $x in $tbl ]
	$cfg := @reverse([$x[1] | $x in $tbl ])
	$res := [ [$date[$i], $cfg[$i]] | $i in @size($date) ]
	return $res
}

/*
$T001 := [[0, "a" ], [4, "b"], [7, "c"], [10, "d"]]
;print (@flatten((@rev_table($T001))))

; ph_table: return a new table starting at ph0
; if $lastdur is <0.0, the duration of the last element is set to the duration of the previous one.
; Ex: ((0 a) (4 b) (7 c) (10 d)) with lastdur = 1 and ph0 = 2
; --> intermediate value ((7 c) (10 d) (11 a) 15 b))
; ---> (0 c) (3 d) (4 a) (8 b)

; Ex: ((0 a) (4 b) (7 c) (10 d)) with lastdur = -1 and ph0 = 2
; --> intermediate value ((7 c) (10 d) (13 a) 17 b))
; ---> (0 c) (3 d) (6 a) (10 b)
*/

@fun_def @ph_table($tbl, $ph0, $lastdur)
{
	if ($ph0 > @size($tbl))
	{
		print "Come on, sir, you ask me to start from " $ph0 "when the table has only " @size($tbl) "elements!"
	}
	else
	{
		@local $currsize, $t1, $t2, $res, $begtime, $per, $currlastdur
		$res := []
		$currsize := @size($tbl)
		$t1 := [ 0 | ($currsize) ]
		if ($lastdur < 0.0)
		{
			$currlastdur := ($tbl[($currsize - 1), 0] - $tbl[($currsize - 2), 0])
		}
		else
		{
			$currlastdur := $lastdur
		}
		;print $currlastdur
		$per := ($tbl[($currsize - 1), 0] + $currlastdur)
		;print $per
		forall $i in $currsize
		{
			$t1[$i] := [($tbl[$i, 0] + $per), $tbl[$i, 1]]
		}
		$t2 := @concat($tbl, $t1)
		$t2 := @rotate($t2, -$ph0)
		$begtime := $t2[0, 0]
		forall $i in $currsize
		{
			@push_back($res, [($t2[$i, 0] - $begtime), $t2[$i, 1]])
		}
		return $res
	}
}

;$T001 := [[0, "a" ], [4, "b"], [7, "c"], [10, "d"]]
;print (@flatten(@ph_table($T001, 2, 0)))
;@flatten(@ph_table([[0, "a" ], [4, "b"], [7, "c"], [10, "d"]], 2, 1)) 
; [[0, "c"], [3, "d"], [8, "a"], [12, "b"]]


;;; TRAJECTORY LOOPER
;	Similar to read, but with loop
; Traj_looper args:
; src	: source ID (an integer number)
; Traj 	: table with trajectories
; dur 	: total duration of the trajectory
; itp 	: interpolation structure
; period 	: 
; totaldur : 

@proc_def ::Traj_looper($src, $Traj, $dur, $itp, $period, $totaldur)
; @abort := { print "abort ::trajLooper" }
{
	@local $stay := 0
	@local $mm := $RT_TEMPO

	if ($dur > $period)
	{ 
		print "duration " $dur " is greater than period " $period  
	}
	else
	{
		@local $durscaler, $currtime, $interdate
	$durscaler :=  $dur / $Traj.last()[0]
	$currtime := ($Traj[0, 0]) * $durscaler
	
	;$source[$src].igoto($stype,$sdur,$itp)
	@Space ($src, ($Traj[0, 1]), $currtime, $stay, $itp, $mm)
	;print $durscaler $currtime
	;print space $src ($Traj[0, 1]) $currtime $itp
     
    @local $lastscaleddur, $j
	$lastscaleddur := ($period - (($Traj.last()[0]) * $durscaler))

     $interdate := [ ($Traj[$i+1, 0] - $Traj[$i, 0])*$durscaler | $i in (@size($Traj)-1)]
     $interdate := @push_back($interdate, $lastscaleddur)

     ;print interdate $interdate

     $j := 0
	loop $interdate ;@tempo := $SCORE_TEMPO
	    @exclusive,
		@abort := { 
			; print "Fading out Traj_looper"
			;@FadeOut($src, 0.1)
			}
	{
		print "traj_loop_inside from " $NOW " with duration " ($interdate[$j % @size($interdate)]) "-- Space_i"
		@Space($src, ($Traj[$j % @size($Traj), 1]), ($interdate[$j % @size($interdate)]), $stay, $itp, $mm)
		$j := $j + 1
	}} 
	$totaldur abort $MYSELF
}

@proc_def ::Traj_looper_t($src, $Traj, $dur, $itp, $period, $tempolocal)  ; takes a tempo argument
{
	@local $stay := 0

	if ($dur > $period)
	{ 
		print "duration " $dur " is greater than period " $period  
	}
	else
	{
		@local $durscaler, $currtime, $interdate
	$durscaler :=  $dur / $Traj.last()[0]
	$currtime := ($Traj[0, 0]) * $durscaler
	
	;$source[$src].igoto($stype,$sdur,$itp)
	@Space($src, ($Traj[0, 1]), $currtime, $stay, $itp, $tempolocal)
	;print $durscaler $currtime
	;print space $src ($Traj[0, 1]) $currtime $itp
     
    @local $lastscaleddur, $j
	$lastscaleddur := ($period - (($Traj.last()[0]) * $durscaler))

     $interdate := [ ($Traj[$i+1, 0] - $Traj[$i, 0])*$durscaler | $i in (@size($Traj)-1)]
     $interdate := @push_back($interdate, $lastscaleddur)

     ;print interdate $interdate
     print $tempolocal

     $j := 0
	loop $interdate  
		@tempo := $tempolocal,
	    @exclusive,
		@abort := { 
			;print "Fading out Traj_looper"
			;@FadeOut($src, 0.1)
			}
	{
		print "traj_loop_inside from " $NOW " with duration " ($interdate[$j % @size($interdate)]) "-- tempolocal" $tempolocal
		@Space($src, ($Traj[$j % @size($Traj), 1]), ($interdate[$j % @size($interdate)]), $stay, $itp, $tempolocal)
		$j := $j + 1
	}} 
}

@proc_def ::Traj_looper_rt($src, $Traj, $dur, $itp, $period) ; takes a tempo argument
{
	@local $stay := 0
	@local $mm := $RT_TEMPO

	if ($dur > $period)
	{ 
		print "duration " $dur " is greater than period " $period  
	}
	else
	{
		@local $durscaler, $currtime, $interdate
	$durscaler :=  $dur / $Traj.last()[0]
	$currtime := ($Traj[0, 0]) * $durscaler
	
	;$source[$src].igoto($stype,$sdur,$itp)
	@Space($src, ($Traj[0, 1]), $currtime, $stay, $itp, $mm)
	;print $durscaler $currtime
	;print space $src ($Traj[0, 1]) $currtime $itp
     
    @local $lastscaleddur, $j
	$lastscaleddur := ($period - (($Traj.last()[0]) * $durscaler))

     $interdate := [ ($Traj[$i+1, 0] - $Traj[$i, 0])*$durscaler | $i in (@size($Traj)-1)]
     $interdate := @push_back($interdate, $lastscaleddur)

     ;print interdate $interdate
     print $RT_TEMPO

     $j := 0
	loop $interdate  
		@tempo := $RT_TEMPO,
	    @exclusive,
		@abort := { 
			;print "Fading out Traj_looper"
			;@FadeOut($src, 0.1)
			}
	{
		;print "traj_loop_inside from " $NOW " with duration " ($interdate[$j % @size($interdate)]) "-- Space_i"
		@Space($src, ($Traj[$j % @size($Traj), 1]), ($interdate[$j % @size($interdate)]), $stay, $itp, $mm)
		$j := $j + 1
	}} 
}


@proc_def ::Traj_looper_st($src, $Traj, $dur, $itp, $period, $totaldur, $tempo, $stay_percent)
@abort := { print "abort ::trajLooper_st" }
{
        ;print ("=========>>>> " + $Traj)

        if ($dur > $period)
        { 
                print "duration " $dur " is greater than period " $period
        }
        else
        {
		@local $start_time := $NOW

		@local $start_tempo := $tempo.min_key()
     	        @local $stop_tempo := $tempo.max_key()
                ;scaled tempo to totaldur
		@local $tempol := $tempo.scale_x($totaldur/($stop_tempo - $start_tempo))
		
                @local $durscaler :=  $dur / $Traj.last()[0]
                @local $currtime := ($Traj[0, 0]) * $durscaler
        
                ;$source[$src].igoto($stype,$sdur,$itp)
                @Space ($src, ($Traj[0, 1]), $currtime, $stay_percent, $itp, $tempol)
                ;print $durscaler $currtime
                ;print space $src ($Traj[0, 1]) $currtime $itp
     
                @local $lastscaleddur := ($period - (($Traj.last()[0]) * $durscaler))

                @local $interdate := [ ($Traj[$i+1, 0] - $Traj[$i, 0])*$durscaler | $i in (@size($Traj)-1)]
                _ := $interdate.push_back($lastscaleddur)

                ;print interdate $interdate

                @local $j := 0
		
                loop $interdate
                @tempo := $tempol
                @exclusive,
                @abort := { ; print "Fading out Traj_looper"
                            ; @FadeOut($src, 0.1)
			  }
                {
			@local $i := $j % @size($interdate)
		        @local $ldate := $NOW - $start_time
			@local $current_tempo := $tempol($ldate)
			@local $ldur := $interdate[$i] * (60. / $current_tempo)
			@local $stay := $ldur * $stay_percent
			@local $real_dur := $ldur - $stay

/*
			if (0 == $j % @size($interdate))
			{ print "==> TrajLooper start cycle" }
			else
			{ print "--> TrajLooper subcycle: " ($j % @size($interdate)) " duration " $ldur }
*/
			
                        ; print "traj_loop_inside from " $NOW " with duration " $real_dur "-- Space_i"
			$stay 
			@Space($src, ($Traj[$i, 1]), $real_dur, $stay, $itp, $current_tempo)
			
                        $j += 1
                }
          }
          $totaldur abort $MYSELF
}




; SPACE VOCABULARY: SEE EACH CONFIG FILE
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



