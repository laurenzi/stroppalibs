; SPATIAL LIBRARY FOR ...of Silence, version 2014
; 140804


//////////////////////////////////////////////////////////

$source := [ [0.0 | (5)] | (10)]		; last element is at $source[10,5]
print The source matrix has dimension @dim($source) with @size($source) coordinates and @size($source[1]) sources  last element is ($source[9,4])

; CLASS SOURCE
@proc_def ::source($id)
{
	@local  $last_action
    $last_action := 0    

	; GENERAL PROCESSES    
	whenever($Ticket[0] == $id)
	{
		abort $last_action
		$last_action := ::FlyTo($Ticket[0], $Ticket[1], $Ticket[2])
	}
	
	whenever ($abort == $id)
	{
		abort $last_action
		$last_action := 0
	}
	; Generic mouvement process
	whenever($Move[0] == $id)
	{
		abort $last_action
		$last_action := ::Move($Move[0], $Move[1], $Move[2])
	}
}

/*
	Generic movement method.
	Input arguments:
		$sourcenum:	Integer identifier for source number
		$destination:	Symbol identifying the destination. MUST BE a member of initlevels MAP
		$dur:		Duration of interpolation
*/
@proc_def ::FlyTo($sourcenum, $destination, $dur)
{
  curve FlyingEngine   @Grain := 0.05s, 
		@Action := 
		{
			let $source[$sourcenum][0] := $x1
			let $source[$sourcenum][1] := $x2
			let $source[$sourcenum][2] := $x3
			let $source[$sourcenum][3] := $x4
			let $source[$sourcenum][4] := $x5
			@command("src"+$sourcenum+"-spat") $x1 $x2 $x3 $x4 $x5
		}
		{
			$x1, $x2, $x3, $x4, $x5
			{
		{ ($source[$sourcenum,0]) ($source[$sourcenum,1]) ($source[$sourcenum,2]) ($source[$sourcenum,3]) ($source[$sourcenum,4])}	@type "sine_in_out"
			$dur   	{ ($initlevels($destination)[0]) ($initlevels($destination)[1]) ($initlevels($destination)[2]) ($initlevels($destination)[3]) ($initlevels($destination)[4]) } @type "sine_in_out"
			}
		}
}

///////////// Modular movement process
; define spatialization break-points
$h1 := [1., 0., 0., 0., 0.]
$h2 := [0., 1., 0., 0., 0.]
$h3 := [0., 0., 1., 0., 0.]
$h4 := [0., 0., 0., 1., 0.]
$h5 := [0., 0., 0., 0., 1.]
$SpeakerArray := [ $h1, $h2, $h3, $h4, $h5]
$typ := ["SINE_IN_OUT" | (5)]


; Arguments are:
;			$sourcenum  : 	Spatialisation source number / integer
;			$source_tab :	movement table / table, as in [1, 2, 3] -- NOTE: Use colons to separate table elements!
;			$dur		:	global duration / float

@macro_def Move($source,$sourcetab, $dur) { $Move := [$source,$sourcetab, $dur] }
@proc_def ::Move($sourcenum, $source_tab, $dur)
{
	@local $interpol				; interpolation map to be constructed from $source_tab
	@local $abstract_duration		; abstract duration (scrub) of the interpolation map
	$abstract_duration := 1.0

	; construct the first element, and then go for others.. 
	;	we have to this since we don't really have types. So for using PUSH_BACK we need to construct atleast the first element!
	;	The interpolation map starts on the last position, and interpolates to the first position in $source_tab
	
	$interpol := NIM { 0 ($source[$sourcenum]), 1.0 ($SpeakerArray[$source_tab[0] - 1]) "SINE_IN_OUT" }

	; now we iterate on the rest of the table (if any) and push elements incrementally
	$index_list := [ $i | $i in 1 .. @size($source_tab) ]

	forall $i in $index_list
	{
		;print constructing nim "for" ($source_tab[$i])
		$abstract_duration := $abstract_duration + 1.0
		; push back the last element into the interpolation map
		$interpol := @push_back($interpol, 1.0, $SpeakerArray[$source_tab[$i] - 1], "SINE_IN_OUT")
	}

	; Now drive the interpolation map using a curve and the given duration.
	; The principle is the following: The NIM is constructed on an x-axis that corresponds to a total $abstract_time
	; The curve is like a header that reads this interpolation map in the given duration.


	curve nimclock @grain:=0.05s,
		@action :=
		{
			let $source[$sourcenum] := $interpol($x)
			@command("src"+$sourcenum+"-spat") ( $interpol($x) )
		}
		{
			$x
			{
				{0.0}
				$dur {$abstract_duration}
			}
		}

}

//////////////////////////////////////////////////////////

; SPACE VOCABULARY

; SINGLE LOUDSPEAKERS
; Front [1], Left [2], Rear [3], Right [4], Vert [5]
; NB: -Lsp = phase opposition (ex. -1)

; DOUBLE LOUDSPEAKERS
; DIPOLES [1] = IN PHASE
; FrontalDipole[1] [1-3] / LateralDipole[1] [2-4]
; HORIZONTAL
; FrontLeftCorner[1] [1-2] / RearLeftCorner[1] [2-3] / RearRightCorner[1] [3-4] / FrontRightCorner[1] [4-1]
; VERTICAL
; FrontVertCorner[1] [5-1] / LeftVertCorner[1] [5-2] / RearVertCorner[1] [5-3] / RightVertCorner[1] [5-3] 
 
; TRIPLE LOUDSPEAKERS
; FrontTriple[1] [1-2 3], LeftTriple[1] [1-2 3], RearTriple[1] [2-3 4], RightTriple[1] [3-4 1]

; QUADRUPLE LOUDSPEAKERS
; AllHorizontal [1-2 3-4]
; AllDipoles [1 2-3-4]
; AllHorizontalinPhase [1 2 3 4]

; AllVertical
; AllinPhase
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;_________________________________________________________________________________________
; General SPACE Macro to assign movements to sources.
; Arguments:
;	1st argument: source number (integer)
;	2nd argument:	movement type (string) --> must be in the $initlevels map. Must be wrapped in quotations
;	3rd argument: duration (float, or time in second or milli-second: ex. 3.0 or 3.0s or 3.0ms)
@macro_def Space($snum, $stype, $sdur) {	$Ticket := [$snum, $stype, $sdur]	}

;FadeOut
@macro_def FadeOut($sourcenum, $dur) { $Ticket := [$sourcenum,"FadeOut", $dur] }
