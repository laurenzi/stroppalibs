BPM 60

; Set target time for all line~ renewal. Must be equal to Curve grain
target-time 50.0

src1-spat 0. 0. 0. 0. 0.

; declare source variables globally so that we can have continuous movements
@global $s11, $s12, $s13, $s14, $s15
$s11:= 0.0
$s12:= 0.0
$s13:= 0.0
$s14:= 0.0
$s15:= 0.0

@MACRO_DEF Fadeout($sourcenum, $dur)
{
	curve Fadeout  @Grain := 0.05s, 
		@Action := 
		{
			@command("src"+$sourcenum+"-spat") $s$sourcenum1 $s$sourcenum2 $s$sourcenum3 $s$sourcenum4 $s$sourcenum5
		}
	{
		$s$sourcenum1, $s$sourcenum2, $s$sourcenum3, $s$sourcenum4, $s$sourcenum5
		{
					{ $s$sourcenum1 $s$sourcenum2 $s$sourcenum3 $s$sourcenum4 $s$sourcenum5 } @type "quad_in_out"
		$dur   		{ 0. 0. 0. 0. 0. } @type "quad_in_out"
		}
	}
}


@MACRO_DEF Clockwise($sourcenum, $dur)
{
	curve Clockwise  @Grain := 0.05s, 
		@Action := 
		{
			@command("src"+$sourcenum+"-spat") $s11 $s12 $s13 $s14 $s15
		}
	{
		$s11, $s12, $s13, $s14, $s15
		{
					{ 1. 0. 0. 0. 0. } @type "quad_in_out"
		$dur/4.0   	{ 0. 1. 0. 0. 0. } @type "quad_in_out"
		$dur/4.0	{ 0. 0. 1. 0. 0. } @type "quad_in_out"
		$dur/4.0 	{ 0. 0. 0. 1. 0. } @type "quad_in_out"
		$dur/4.0 	{ 1. 0. 0. 0. 0. } @type "quad_in_out"
		}
	}
}

@MACRO_DEF LateralDipole($sourcenum, $dur)
{
	if ($dur=0.0)
	{
		$s11:= 1.0
		$s12:= 0.0
		$s13:= -1.0
		$s14:= 0.0
		$s15:= 0.0
		@command("src"+$sourcenum+"-spat") $s11 $s12 $s13 $s14 $s15
	}else
	{
	curve toLateralDipole  @Grain := 0.05s, 
		@Action := 
		{
			@command("src"+$sourcenum+"-spat") $s11 $s12 $s13 $s14 $s15
		}
	{
		$s11, $s12, $s13, $s14, $s15
		{
					{ $s11 $s12 $s13 $s14 $s15 } @type "quad_in_out"
		$dur   		{ 1. 0. -1.0 0. 0. } @type "quad_in_out"
		}
	}
	}
}

@MACRO_DEF HorizontalDipole($sourcenum, $dur)
{
	if ($dur=0.0)
	{
		$s11:= 0.0
		$s12:= 1.0
		$s13:= 0.0
		$s14:= -1.0
		$s15:= 0.0
		@command("src"+$sourcenum+"-spat") $s11 $s12 $s13 $s14 $s15
	}else
	{
	curve toHorzDipole  @Grain := 0.05s, 
		@Action := 
		{
			@command("src"+$sourcenum+"-spat") $s11 $s12 $s13 $s14 $s15
		}
	{
		$s11, $s12, $s13, $s14, $s15
		{
						{ $s11 $s12 $s13 $s14 $s15 } @type "quad_in_out"
			$dur   		{ 0. 1. 0. -1. 0. } @type "quad_in_out"
		}
	}
	}
}

@MACRO_DEF ClockwiseDipole($sourcenum, $dur)
{
	curve ClockwiseDipole  @Grain := 0.05s, 
		@Action := 
		{
			@command("src"+$sourcenum+"-spat") $s11 $s12 $s13 $s14 $s15
		}
	{
		$s11, $s12, $s13, $s14, $s15
		{
						{ 1. 0. -1. 0. 0. } @type "quad_in_out"
			$dur/2.0   	{ 0. 1. 0. -1. 0. } @type "quad_in_out"
			$dur/2.0	{ 1. 0. -1. 0. 0. } @type "quad_in_out"
		}
	}
}

@MACRO_DEF Vertical($sourcenum, $dur)
{
	curve Vertical  @Grain := 0.05s, 
		@Action := 
		{
			@command("src"+$sourcenum+"-spat") ($s+$sourcenum+"1") ($s+$sourcenum+"2") ($s+$sourcenum+"3") ($s+$sourcenum+"4") ($s+$sourcenum+"5")
		}
	{
		$s11, $s12, $s13, $s14, $s15
		{
					{ $s11 $s12 $s13 $s14 $s15 } @type "quad_in_out"
			$dur   	{ 0. 0. 0. 0. 1. } @type "quad_in_out"
		}
	}
}


NOTE C4 5.0 ClockwiseMoves

@Clockwise(1, 4.0)

@ClockwiseDipole(1, 4.0)

@LateralDipole(1, 0.0)

@Fadeout(1,0.1)

NOTE D4 5.0 Dipoles&Vertical

@LateralDipole(1, 3.0)

@HorizontalDipole(1, 3.0)

@Vertical(1, 3.0)

@Vertical(1, 0.0)

NOTE E4 5.0 Looping

loop clock 4.0
{
	@Clockwise(1,4.0)
}

abort clock
@Fadeout(1,0.1)



