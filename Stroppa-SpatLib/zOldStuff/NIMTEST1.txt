
NOTE G4 10.0  basic_NIMS


;this should create an Interpolated MAP which is
; define spatialization break-points
$h1 := [1., 0., 0., 0., 0.]
$h2 := [0., 1., 0., 0., 0.]
$h3 := [0., 0., 1., 0., 0.]
$h4 := [0., 0., 0., 1., 0.]
$h5 := [0., 0., 0., 0., 1.]
$typ := ["SINE_IN_OUT" | (5)]




;$interpol := [ NIM { 0 $h2[$i], 1.0 $h3[$i] "SINE_IN_OUT", 2.0 $h5[$i] "SINE_IN_OUT" } | $i in @size($h1) ]

$interpol := NIM { 0 $h2, 1.0 $h3 }    ; $typ

print test

$interpol := @push_back($interpol, 2.0, $h5, "SINE_IN_OUT") ; $typ

print hey $interpol


/*curve toto @grain:=0.05s,
	@action :=
	{
		src1-spat ( $interpol($x) )
	}
	{
		$x
		{
			{0.0}
			3.0 {3.0}
		}
	}*/



NOTE G4 10.0  toto
