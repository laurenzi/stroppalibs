// SourceControl Patcher Creater
// Written by Arshia Cont
// June 2016
// See https://forge.ircam.fr/p/StroppaLibs/doc/ for more details

// Inlets and outlets
inlets = 1;
outlets = 0;

// global variables
var numPoints=4;	// Number of Speakers
var numSources=2;
var targetTime=30;

// parent elements
var thedac;
var theAudioSenders = new Array(64);
var theAudioReceivers = new Array(64);
var theMaininlets = new Array(64);
var theMainoutlets = new Array(64);
var SourcePatchers = new Array(64);

// subpatcher elements
var thereceiver;
var theunpack;
var AudioInlet;
var theoutlets = new Array(64);
var themultipliers = new Array(64);
var thelines = new Array(64);
var thepacks = new Array(64);

//// Instantiation arguments
// Arguments in order are:  #ofSources #ofSpeakers TargetTime
if (jsarguments.length>=4)
{
	// Third argument is targetTime
	targettime(jsarguments[3]);
	// 2nd argument is #ofSpeakers
	numSpeakers(jsarguments[2]);
	// 1st argument is numofSources (that builds the patch)
	numSource(jsarguments[1]);
}



function numSource(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];

		// safety check for number of speajers
		if(a<1) a = 1; // too few speakers, set to 0		
		
		// Delete older stuff if any
		for (k=0;k<numSources;k++)
		{
			this.patcher.remove(theMaininlets[k]);
			this.patcher.remove(SourcePatchers[k]);
			this.patcher.remove(theAudioReceivers[k]);
		}
		for (k=0; k<numPoints;k++)
		{
			this.patcher.remove(theMainoutlets[k]);
			this.patcher.remove(theAudioSenders[k]);
		}
		
		// Create patchers
		numSources = a;
		for (k=0; k<numPoints;k++)
		{
			theMainoutlets[k] = this.patcher.newdefault(50+(k*100),300, "outlet");
			var hpout='hp'+(k+1)+'-out';
			theAudioSenders[k] = this.patcher.newdefault(30+(k*100),250, "send~", hpout);
		}
		for (u=0; u<numSources; u++)
		{
			var snum='Source'+(u+1);
			var snumr='src'+(u+1)+'-in';
			theMaininlets[u] = this.patcher.newdefault(20+(u*100),100, "inlet");
			theAudioReceivers[u]= this.patcher.newdefault(30+(u*100),125, "receive~", snumr);
			SourcePatchers[u] = this.patcher.newdefault(20+(u*100),150, "patcher", snum);
			SourcePatchers[u].subpatcher().wind.visible=0;	// hide them!
			SourceCreator(numPoints, SourcePatchers[u].subpatcher(), (u+1) );
			
			// Create connections
			this.patcher.connect(theMaininlets[u],0,SourcePatchers[u],0);
			this.patcher.connect(theAudioReceivers[u],0,SourcePatchers[u],0);
			for (k=0; k<numPoints;k++)
			{
				this.patcher.connect(SourcePatchers[u], k, theMainoutlets[k], 0);
				this.patcher.connect(SourcePatchers[u], k, theAudioSenders[k], 0);
			}
		}	
	}else
	{
		post("numSources message needs arguments");
		post();
	}
}

function numSpeakers(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];

		// safety check for number of speajers
		if(a<2) a = 2; // too few speakers, set to 0		
		numPoints = a;
	}else
	{
		post("numSources message needs arguments");
		post();
	}
}

function SourceCreator(speakers, thispatcher, sourceNumber)
{
	if (true) // bail if no argument
	{
		// delete prior created patchers
		/*this.patcher.remove(thereceiver);
		this.patcher.remove(AudioInlet);
		this.patcher.remove(theunpack);
		for (k=0;k<numSpeakers;k++)
		{
			this.patcher.remove(thepacks[k]);
			this.patcher.remove(thelines[k]);
			this.patcher.remove(themultipliers[k]);
			this.patcher.remove(theoutlets[k]);
		}*/
		
		// create new patcher
		//numSpeakers = speakers;		// update global variable
		
		var sourceString = 'src'+sourceNumber+'-spat';
		
		thereceiver = thispatcher.newdefault(100, 20, "receive", sourceString);
		AudioInlet = thispatcher.newdefault(50, 50, "inlet");
		theunpack = thispatcher.newdefault(100, 50, "unjoin", numPoints);
		thispatcher.connect(thereceiver, 0, theunpack, 0);
		for (i=0; i<numPoints;i++)
		{
			thepacks[i] = thispatcher.newdefault(100+(i*100), 100, "pack", 0.1, targetTime);
			thelines[i] = thispatcher.newdefault(100+(i*100), 150, "line~");
			themultipliers[i] = thispatcher.newdefault(100+(i*100), 200, "*~", 0.0); 
			theoutlets[i] = thispatcher.newdefault(100+(i*100), 250, "outlet");
			thispatcher.connect(thelines[i], 0, themultipliers[i], 1);
			thispatcher.connect(AudioInlet, 0, themultipliers[i], 0);
			thispatcher.connect(thepacks[i], 0, thelines[i], 0);
			thispatcher.connect(theunpack, i, thepacks[i], 0);
			thispatcher.connect(themultipliers[i], 0, theoutlets[i], 0);
		}
		
	}else
	{
		post("speakers message needs arguments");
		post();
	}
}

function targettime(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];

		// safety check for number of speajers
		if(a<10) a = 10; // too few speakers, set to 0
		if(a>300) a = 300; // too many speakers, set to 64
		
		targetTime = a;
	}else
	{
		post("targettime message needs arguments");
		post();
	}
}