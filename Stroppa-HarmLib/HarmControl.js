// HarmControl Patcher Creater
// Written by Arshia Cont
// June 2016
// See https://forge.ircam.fr/p/StroppaLibs/doc/ for more details

// Inlets and outlets
inlets = 1;
outlets = 0;

// global variables
var numHarms = 4;
var theWindowSize = 4096;
var theOverlapFactor = 4;
var theNameSpace = 'hr';

// parent elements
var theMainInlet;
var theMainOutlet;
var theReceiveTildes = new Array(64);
var theSendTildes = new Array(64);
var HarmPatchers = new Array(64);
var MuteToggles = new Array(64);
var GlobalGainReceive;
var GlobalGainMult;
var GlobalGainMess;
var GlobalGainLine;
var theSendTilde;
var theDbConvertor;

// subpatcher elements
var AudioInlet = new Array(64);
var AudioOutlet = new Array(64);
var thePffts = new Array(64);
var theIntervalReceivers = new Array(64);
var theGainReceivers = new Array(64);
var theDbConvertors = new Array(64);

//// Instantiation arguments
// Arguments in order are:  #ofSources #ofSpeakers TargetTime
if (jsarguments.length>=4)
{
	NameSpace(jsarguments[3]);
	// 2nd argument is windowsize
	WindowSize(jsarguments[2]);
	// 1st argument is numofSources (that builds the patch)
	Harms(jsarguments[1]);
}

function Harms(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];
		
		// safety check for number of harms
		if(a<1) a = 1; // too few harms, set to 1		
		
		// Delete older stuff if any
		this.patcher.remove(theMainInlet);
		this.patcher.remove(theMainOutlet);
		this.patcher.remove(GlobalGainMult);
		this.patcher.remove(GlobalGainReceive);
		this.patcher.remove(GlobalGainMess);
		this.patcher.remove(GlobalGainLine);
		this.patcher.remove(theSendTilde);
		this.patcher.remove(theDbConvertor);
		for (k=0;k<numHarms;k++)
		{
			this.patcher.remove(HarmPatchers[k]);
			this.patcher.remove(MuteToggles[k]);
			this.patcher.remove(theReceiveTildes[k]);
			this.patcher.remove(theSendTildes[k]);
		}
		
		// Create patcher
		numHarms = a;
		theMainInlet = this.patcher.newdefault(20,100, "inlet");
		theMainOutlet = this.patcher.newdefault(20,300, "outlet");
		var sendt = theNameSpace+'_out';
		theSendTilde = this.patcher.newdefault(100, 300, "send~", sendt);
		// Global Gain Control
		GlobalGainReceive = this.patcher.newdefault(500,25, "receive", 'hr_out_gain');
		theDbConvertor = this.patcher.newdefault(500, 50, "dbtoa");
		GlobalGainMess = this.patcher.newobject("message",500, 75,90,15, "$1 30");
		GlobalGainLine = this.patcher.newdefault(500, 100, "line~");
		GlobalGainMult = this.patcher.newdefault(20,250, "*~", 0.01);
		this.patcher.connect(GlobalGainReceive, 0, theDbConvertor, 0);
		this.patcher.connect(theDbConvertor, 0, GlobalGainMess, 0);
		this.patcher.connect(GlobalGainMess, 0, GlobalGainLine, 0);
		this.patcher.connect(GlobalGainLine, 0, GlobalGainMult, 1);
		for (i=0; i<numHarms; i++)
		{
			var sin = theNameSpace+(i+1)+'_in'; // receive~ input name
			var sout = theNameSpace+(i+1)+'_out'; // send~ output name
			theReceiveTildes[i] = this.patcher.newdefault(20+(i*100),150, "receive~", sin);
			var snum = theNameSpace+(i+1); // patcher name
			HarmPatchers[i] = this.patcher.newdefault(20+(i*100),175, "patcher", snum);
			HarmPatchers[i].subpatcher().wind.visible=0;	// hide them!
			HarmCreator(HarmPatchers[i].subpatcher(), i );
			MuteToggles[i] = this.patcher.newdefault(100+(i*100), 200, "toggle");
			theSendTildes[i] = this.patcher.newdefault(20+(i*100),200, "send~", sout);

			// Create connections
			this.patcher.connect(theReceiveTildes[i], 0, HarmPatchers[i], 0);
			this.patcher.connect(theMainInlet, 0, HarmPatchers[i], 0);
			this.patcher.connect(HarmPatchers[i], 0, GlobalGainMult, 0);
			this.patcher.connect(HarmPatchers[i], 0, theSendTildes[i], 0);
			this.patcher.connect(HarmPatchers[i], 1, MuteToggles[i], 0);
			this.patcher.connect(GlobalGainMult, 0, theMainOutlet, 0);
			this.patcher.connect(GlobalGainMult, 0, theSendTilde, 0);
		}		
		
	}else
	{
		post("Harms message needs arguments");
		post();
	}
}

function HarmCreator(thispatcher, u)
{
	// u refers to index; so harmnum is u+1
	AudioInlet[u] = thispatcher.newdefault(20, 20, "inlet");
	AudioOutlet[u] = thispatcher.newdefault(20, 350, "outlet");
	thePffts[u] = thispatcher.newdefault(20, 150, "pfft~", 'gizmo_loadme', theWindowSize, 4);
	thispatcher.connect(AudioInlet[u], 0, thePffts[u], 0);
	
	// build variable names
	var interval_var = theNameSpace+(u+1)+'_interval';
	var gain_var = theNameSpace+(u+1)+'_gain';
	theIntervalReceivers[u] = thispatcher.newdefault(100, 20, "receive", interval_var);
	theGainReceivers[u] = thispatcher.newdefault(50, 175, "receive", gain_var);
	theDbConvertors[u] = thispatcher.newdefault(50, 200, "dbtoa");
	
	// Gain control
	var messi = thispatcher.newobject("message",50, 225,90,15, "$1 30");
	var linet = thispatcher.newdefault(50, 250, "line~");
	var multip = thispatcher.newdefault(20, 275, "*~", 0.01);
	thispatcher.connect(thePffts[u], 0, multip, 0);
	thispatcher.connect(theGainReceivers[u], 0, theDbConvertors[u], 0);
	thispatcher.connect(theDbConvertors[u], 0, messi, 0);
	thispatcher.connect(messi, 0, linet, 0);
	thispatcher.connect(linet, 0, multip, 1);
	thispatcher.connect(multip, 0, AudioOutlet[u], 0);
	
	// Interval control
	var routo = thispatcher.newdefault(100, 50, "route", 'mute');
	//var messo = thispatcher.newobject("message",100, 75,90,15, 'mute $1');
	var messo = thispatcher.newdefault(100, 75, "prepend", "mute");
	thispatcher.connect(theIntervalReceivers[u], 0, routo, 0);
	thispatcher.connect(routo, 0, messo, 0);
	thispatcher.connect(routo, 1, thePffts[u], 1);
	thispatcher.connect(messo, 0, thePffts[u], 0);
	var toggleoutlet = thispatcher.newdefault(150, 100, "outlet");
	thispatcher.connect(routo, 0, toggleoutlet, 0);
}


function WindowSize(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];

		// safety check
		if(a<512) a = 512; 		
		theWindowSize = a;
	}else
	{
		post("WindowSize message needs arguments");
		post();
	}
}

function NameSpace(val)
{
	if (arguments.length) // bail if no argument
	{
		// parse arguments
		a = arguments[0];

		theNameSpace = a;
	}else
	{
		post("SendName message needs arguments");
		post();
	}
}